
// Pon en un div el siguiente texto: “Al pasar por encima de este texto cambiará de tamaño”.
// Este texto inicialmente tendrá un tamaño de 12pt, cuando pasemos por encima de él,
// aumentará a 16 pt y cuando salgamos volverá al tamaño original. Haz el ejercicio utilizando
// funciones externas y “this”.


function bigText(tag)
{
	tag.firstElementChild.style.fontSize = "16pt";
}

function smallText(tag)
{
	tag.firstElementChild.style.fontSize = "12pt";
}


// Haz el ejercicio anterior pero utilizando la estructura semántica para construir y llamar al
// evento.


var ex2 = document.getElementById("ex-2");

ex2.onmouseover = function() {this.firstElementChild.style.fontSize = "16pt";};
ex2.onmouseout = function() {this.firstElementChild.style.fontSize = "12pt";};


// Realiza el mismo ejercicio utilizando el objeto event para que en función del tipo de evento
// aumente o disminuya el tamaño de la letra.


function changeText(inEvent)
{

	var evt = inEvent || window.event;

	switch(evt.type)
	{
		case "mouseover":

			this.style.fontSize = "16pt";
			break;

		case "mouseout":

			this.style.fontSize = "12pt";
			break;
	}

}

var ex3 = document.getElementById("ex-3");

ex3.onmouseover = changeText;
ex3.onmouseout = changeText;


// Por último realizamos el ejercicio con attachEvent y con addEventListener. Utiliza algún
// método o función para que sirva a la vez para Internet Explorer y para el resto de navegadores.


var ex4 = document.getElementById("ex-4");

ex4.addEventListener("mouseover", changeText, false);
ex4.addEventListener("mouseout", changeText, false);


// Crea un formulario con un campo de texto que sólo permita la introducción de números.
// Debéis utilizar el evento onKeypress y las propiedades keyCode y charCode para controlar que
// los valores introducidos sean correctos.

var numbers = document.getElementById("numbers");

numbers.onkeypress = function(e)
	{
		e = e || window.event;

		var charCode = (typeof e.which == "number") ? e.which : e.keyCode;

		if ((charCode < 48 || charCode > 57) && (charCode != 8 && charCode != 0))
		{

			alert("Solo numeros");

			e.preventDefault();

		}

	};

// Crear una página con varios botones para que cada vez que se pulse en uno de ellos salga
// el mensaje "Has pulsado sobre un botón"(no sabemos el número de botones que puede haber,
// no se puede hacer de forma estática poniendo cada uno los botones que quiera y su onclick,
// sino que hay que hacerlo para cualquier página).

function buttonAlert()
{
	alert("Has pulsado sobre un botón");
}

var buttonContainer = document.getElementById("ex-6");
var numButtons = Math.floor(Math.random() * (10 - 1)) + 1;

for (var i = 0; i < numButtons; i++)
{

	button = document.createElement("button");
	button.appendChild(document.createTextNode("Boton " + (i+1)))
	button.className = "button-ex-6";

	buttonContainer.appendChild(button);

};

var buttonArray = document.getElementsByTagName("button");

for (var i = buttonArray.length - 1; i >= 0; i--)
{

	buttonArray[i].addEventListener("click", buttonAlert, false);

};

// Siguiendo el ejemplo del tema 8 (punto 8.1-Relojes,contadores e intervalos de tiempo), crea
// una página que muestre el reloj creado. Además debe aparecer inicialmente cuándo se cargue
// la página un mensaje (con alert) que nos diga “Página que contiene un reloj digital”, también
// controlaremos que se si se abandona la página(por ejemplo volviendo al home) debe aparecer
// un mensaje (con alert) con el mensaje “Página cancelada”.

function updateClock ()
{

	var newDate = new Date();
	var newHour = newDate.toTimeString();

	replacement = document.createElement("p");
	replacement.appendChild(document.createTextNode(newHour.substr(0, 8)));

	clock.replaceChild(replacement, clock.firstElementChild);

}

var clock = document.getElementById("clock");

updateClock();

var clockInterval = window.setInterval(updateClock, 1000);

alert("Pagina que contiene un reloj digital");

window.addEventListener("beforeunload", function (evt) {
  var message = "Pagina cancelada";

  (evt || window.event).returnValue = message;
  return message;
});