// Crear un formulario con tres campos. Cancela el envío del mismo mientras haya algún campo vacío
// Muestra un mensaje de la cancelación del envío y otro cuando este se produzca.

var form = document.getElementById("ex-1");

function exercise1(evt)
{

	evt = evt || window.event;

	var fields = document.getElementsByClassName("form-input");
	var success = true;

	for (var i = fields.length - 1; i >= 0; i--)
	{
		if(fields[i].value == '') {success = false;}
	};

	if (success)
	{
		alert("Formulario enviado");
	}
	else
	{
		alert("Formulario no enviado");

		if (evt.preventDefault())
		{
			evt.preventDefault()
		}
		else
		{
			return false;
		}

	};

}

form.addEventListener("submit", exercise1, false);

// Crea un documento con un link . Evita que se produzca el comportamiento normal y que al hacer clic
// sobre el enlace este no vaya al sitio indicado.

var link = document.getElementById("google");

function exercise2(evt)
{

	if (evt.preventDefault())
	{
		evt.preventDefault()
	}
	else
	{
		return false;
	}

}

link.addEventListener("click", exercise2, false);