function ex01() {

  var input = document.getElementById("key-1");
  var regexp = /^[A-Z]{1}[0-9]{3}/;

  if(regexp.test(input.value)) {

    alert("La clave es correcta");

  } else {

    alert("La clave es incorrecta");
    input.focus();

  }

}

function ex02() {

  var dateContainer = document.getElementById("ex-2");
  var p = document.createElement("p");
  var date = new Date();

  p.appendChild(document.createTextNode(date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear()));
  dateContainer.appendChild(p);

}

function ex03() {

  var container = document.getElementById("contenedor");

  check = false;

  for (var i = container.children.length - 1; i >= 0; i--) {

    if(container.children[i].nodeType != 3) {

      check = true;
      break;

     }

  };

  if (check) {

    var par = prompt("¿Que parrafo desea eliminar?");
    var children = container.children;
    container.removeChild(children[Number(par)-1]);


  } else {

    alert("No hay mas parrafos para borrar");

  }

}

function ex04() {

  var form = document.getElementById("exercise-4");
  var sum = 0;

  for (i = 0; i < form.media.length; i++) {

    if (form.media[i].checked){

      sum += Number(form.media[i].value);

    }

  }

  for (var i = 0; i < form.cpu.length; i++){

    if (form.cpu[i].checked){

      sum += Number(form.cpu[i].value);
      break;

    }

  }

  alert("El presupuesto asciende a " + sum);

}

var button1 = document.getElementById("submit-1");
var button3 = document.getElementById("submit-3");
var button4 = document.getElementById("submit-4");

button1.addEventListener("click", ex01, false);
button3.addEventListener("click", ex03, false);
button4.addEventListener("click", ex04, false);

ex02();