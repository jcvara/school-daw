function ej01()
{

	// Introducir 5 números. Calcular la suma de todos ellos.
	// Decir cuántos de ellos son mayores de 100.

	var numPrompt;
	var numArray;
	var numSum = 0;
	var numOver = "";

	numPrompt = prompt("Introduce cinco numeros, separados por espacios:");

	numArray = numPrompt.split(" ");

	for (i in numArray) {

		numSum += parseInt(numArray[i]);

		if (numArray[i] > 100)
		{
			numOver += numArray[i];
			numOver += " ";
		};
	};

	alert(
		"La suma de los numeros es: " + numSum + "\n\n" +
		"Los numeros mayores de 100 son: " + numOver
	);

}

function ej02()
{

	// Almacena en un array los números 7,8,2,9,10.
	// Calcular la suma de los números mayores de 8.

	var numArray = [7, 8, 2, 9, 10];
	var numSum = 0;

	for (i in numArray)
	{
		if (numArray[i] > 8)
		{
			numSum += numArray[i];
		};
	}

	alert("La suma es: " + numSum);

}

function ej03()
{

	// Introducir un mes y decir a que estación pertenece:
	//
	// Ej:
	//
	// Dime un mes: enero
	//
	// Estamos en Invierno
	//
	// (Nota: No hace falta ninguna comprobación. Los meses diciembre,enero,febrero serán
	// invierno, marzo,abril y mayo serán primavera, junio,julio y agosto serán verano y
	// septiembre,octubre y noviembre será otoño).

	var mes;

	mes = prompt("Introduce un mes:");

	if (mes.toLowerCase() == "diciembre" || mes.toLowerCase() == "enero" || mes.toLowerCase() == "febrero")
	{
		alert("Estamos en invierno");
	};

	if (mes.toLowerCase() == "marzo" || mes.toLowerCase() == "abril" || mes.toLowerCase() == "mayo")
	{
		alert("Estamos en primavera");
	};

	if (mes.toLowerCase() == "junio" || mes.toLowerCase() == "julio" || mes.toLowerCase() == "agosto")
	{
		alert("Estamos en verano");
	};

	if (mes.toLowerCase() == "septiembre" || mes.toLowerCase() == "octubre" || mes.toLowerCase() == "noviembre")
	{
		alert("Estamos en otoño");
	};

}

function ej04()
{

	// Introducir un número y decir si es par o impar.

	var num;

	num = prompt("Introduce un numero:");

	if(parseInt(num) % 2 == 0)
	{
		alert("El numero es par");
	}
	else
	{
		alert("El numero es impar");
	}

}

function ej05()
{

	// Introducir el precio de un articulo.
	// Calcular el iva y el precio total(con el iva incluido) que debe salir redondeado a dos decimales.

	var precio;
	var iva;
	var total;

	precio = parseFloat(prompt("Introduzca el precio del articulo"));

	iva = precio * 0.21;
	total = precio + iva;

	alert(
		"Precio del articulo: " + precio.toFixed(2) + "€\n" +
		"IVA del articulo " + iva.toFixed(2) + "€\n" +
		"Precio total: " + total.toFixed(2) + "€"
	);

}