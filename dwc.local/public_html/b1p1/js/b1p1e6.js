// Introducir una cadena. Decir la longitud de la cadena. Pasarla a mayúsculas y a
// minúsculas. Mostrar cada palabra de la cadena por separado primero normal y después
// al revés.
//
// Ej:
//
// Introduce una cadena: La casa de Maria
// La longitud de la cadena es 16 caracteres
// La cadena en mayúsculas es LA CASA DE MARIA
// La cadena en minúsculas e s la casa de maria
//
// CADENA NORMAL
//
// La
// casa
// Página 4 de 5de
// Maria
//
// CADENA AL REVÉS
//
// Maria
// de
// casa
// La
//
// (Nota: en el resto de ejercicios para mostrar los resultados gastaréis alert, en este último
// hay que utilizar el document.write).

var cadena;
var longitud;
var mayusculas;
var minusculas;
var palabras = new Array();
var reves = new Array();

cadena = prompt("Introduzca una frase:");

longitud = cadena.length;

mayusculas = cadena.toUpperCase();

minusculas = cadena.toLowerCase();

palabras = cadena.split(" ").reverse();

reves = cadena.split(" ");

document.writeln("<p>La frase tiene " + longitud + " caracteres</p>");
document.writeln("<p>La frase en mayusculas: " + mayusculas + "</p>");
document.writeln("<p>La frase en minusculas: " + minusculas + "</p>");
document.writeln("<p>Las palabras son: <br />");

for (var i = palabras.length - 1; i >= 0; i--) {
	document.writeln(palabras[i] + "<br />");
};

document.writeln("</p>");
document.writeln("<p>Las palabras al reves son: <br />");

for (var i = reves.length - 1; i >= 0; i--) {
	document.writeln(reves[i] + "<br/>");
};
document.writeln("</p>");

document.close();