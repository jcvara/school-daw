
// Crea un elemento (el que tú quieras, por ejemplo una caja de texto) y dale
// atributos.

var exercise = document.getElementById("exercise");
var paragraph = document.createElement("p");

var blueButton = document.getElementById("button-blue");
var redButton = document.getElementById("button-red");

paragraph.setAttribute("class", "paragraph");
paragraph.appendChild(document.createTextNode("Elemento generado a traves de javascript"));

exercise.appendChild(paragraph);

//alert(document.getElementsByClassName("paragraph").length);

function blue()
{
	paragraphList = document.getElementsByClassName("paragraph");
	paragraphList[0].setAttribute("id", "blue");
};

function red()
{
	paragraphList = document.getElementsByClassName("paragraph");
	paragraphList[0].setAttribute("id", "red");
}

blueButton.addEventListener("click", blue, false);
redButton.addEventListener("click", red, false);