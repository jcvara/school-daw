// Crea una página HTML que contenga varias imágenes y enlaces. La página
// tendrá un botón con el texto “Número de Imágenes” y otro con el texto “Número de
// enlaces”. Al pinchar sobre el primer botón debe salir un mensaje con alert que nos
// diga el número total de imágenes que tiene la página y al pinchar sobre el segundo
// botón debe salir un mensaje con alert que nos diga el número total de enlaces.

var imgButton = document.getElementById("images");
var linkButton = document.getElementById("links");

function imgCount()
{
	alert("Hay " + document.getElementsByTagName("img").length + " imagenes en la pagina.");
}

function linkCount()
{
	alert("Hay " + document.getElementsByTagName("a").length + " enlaces en la pagina.");
}

imgButton.addEventListener("click", imgCount, false);
linkButton.addEventListener("click", linkCount, false);
