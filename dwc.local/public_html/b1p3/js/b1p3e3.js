
// Crea una página HTML con un div con el texto “ Bienvenido a esta página de
// prueba de colores” que aparecerá inicialmente en blanco. La página contendrá tres
// botones y al pinchar sobre ellos cambiará el fondo del div.
//
// Azul: Cambiará el fondo de la pantalla a azul.
// Verde: Cambiará el fondo de la pantalla a verde.
// Restaurar color inicial: Dejará el fondo de la pantalla a blanco.

var azul = document.getElementById("azul");
var verde = document.getElementById("verde");
var restaurar = document.getElementById("restaurar");

azul.addEventListener("click", function(){document.getElementById("exercise").style.backgroundColor="Blue";}, false);
verde.addEventListener("click", function(){document.getElementById("exercise").style.backgroundColor="Green";}, false);
restaurar.addEventListener("click", function(){document.getElementById("exercise").style.backgroundColor="Initial";}, false);