
// Crear una página HTML con un párrafo inicial con el texto “Este es mi primer
// párrafo” y cuatro botones.
//
// • Añadir párrafo: Añadirá el párrafo con el texto “Párrafo añadido” detrás del
//   último párrafo creado.
//
// • Insertar párrafo: Insertará el párrafo con el texto “Párrafo insertado” detrás
//   del primer párrafo (“Este es mi primer párrafo”).
//
// • Reemplazar párrafo: Cambiará el texto del segundo párrafo (el siguiente a
//   “Este es mi primer párrafo”) por “Párrafo reemplazado”.
//
// • Borrar párrafo: Eliminará el párrafo que haya detrás de “Este es mi primer
//   párrafo”.
//
// • Clonar div: Duplicará (clonará) toda la estructura div que tengamos creada en
//   el momento que pinchemos el botón.

var addButton = document.getElementById("add");
var insertButton = document.getElementById("insert");
var replaceButton = document.getElementById("replace");
var deleteButton = document.getElementById("delete");
var cloneButton = document.getElementById("clone");

var exercise = document.getElementById("exercise");
var mainBody = document.getElementById("body");

var addition;
var replacement;
var clonedExercise;

function addParagraph ()
{
	exercise.appendChild(document.createElement("p")).appendChild(document.createTextNode("Parrafo añadido"));
}

function insertParagraph ()
{
	addition = document.createElement("p");
	addition.appendChild(document.createTextNode("Parrafo insertado"));
	exercise.insertBefore(addition, exercise.firstElementChild.nextElementSibling);
}

function replaceParagraph ()
{
	replacement = document.createElement("p");
	replacement.appendChild(document.createTextNode("Parrafo reemplazado"));
	exercise.replaceChild(replacement, exercise.firstElementChild.nextElementSibling);
}

function deleteParagraph ()
{
	if(exercise.firstElementChild.nextElementSibling)
	{
		exercise.removeChild(exercise.firstElementChild.nextElementSibling);
	}
}

function cloneStructure ()
{
	clonedExercise = exercise.cloneNode(true);
	exercise.parentNode.appendChild(clonedExercise);
}

addButton.addEventListener("click", addParagraph, false);
insertButton.addEventListener("click", insertParagraph, false);
replaceButton.addEventListener("click", replaceParagraph, false);
deleteButton.addEventListener("click", deleteParagraph, false);
cloneButton.addEventListener("click", cloneStructure, false);
