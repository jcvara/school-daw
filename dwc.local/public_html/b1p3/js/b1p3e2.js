
// Crear una página HTML con una lista desordenada inicial con tres animales (ver
// captura de pantalla). Los botones funcionarán igual que en el ejercicio anterior pero
// con los elementos de la lista(el nombre del animal a añadir o reemplazar es libre).
//
// El botón “Crear Sublista dentro de león con funciones DOM” creará una nueva lista
// desordenada dentro del primer elemento (ver captura de pantalla). El botón “Crear
// Sublista dentro de león con InnerHTML” hará lo mismo que el anterior pero
// utilizando esta propiedad.

var addButton = document.getElementById("add");
var insertButton = document.getElementById("insert");
var replaceButton = document.getElementById("replace");
var deleteButton = document.getElementById("delete");
var cloneButton = document.getElementById("clone");
var subDomButton = document.getElementById("sub-dom");
var subHtmlButton = document.getElementById("sub-html");

var animals = document.getElementById("animals");
var exercise = document.getElementById("exercise");

var addition;
var insertion;
var replacement;
var clonedExercise;
var subList;
var subItem;

function addAnimal()
{
	addition = document.createElement("li");
	addition.appendChild(document.createTextNode("Coyote"));
	animals.appendChild(addition);
}

function insertAnimal()
{
	addition = document.createElement("li");
	addition.appendChild(document.createTextNode("Dingo"));
	animals.insertBefore(addition, animals.firstElementChild.nextElementSibling);
}

function replaceAnimal()
{
	replacement = document.createElement("li");
	replacement.appendChild(document.createTextNode("Lobo"));
	animals.replaceChild(replacement, animals.firstElementChild.nextElementSibling);
}

function deleteAnimal()
{
	if(animals.firstElementChild.nextElementSibling)
	{
		animals.removeChild(animals.firstElementChild.nextElementSibling);
	}
}

function cloneStructure()
{
	clonedExercise = animals.cloneNode(true);
	exercise.appendChild(clonedExercise);
}

function addDom()
{
	if (!animals.firstElementChild.firstElementChild)
	{
		subList = document.createElement("ul");
		subItem = document.createElement("li");
		subItem.appendChild(document.createTextNode("Zorro"));
		subList.appendChild(subItem);
		animals.firstElementChild.appendChild(subList);
	}
	else
	{
		subList = animals.firstElementChild.firstElementChild;
		subItem = document.createElement("li");
		subItem.appendChild(document.createTextNode("Zorro"));
		subList.appendChild(subItem);
	}
}

function addHtml()
{
	if (!animals.firstElementChild.firstElementChild)
	{
		animals.firstElementChild.innerHTML += "<ul><li>Zorro</li></ul>";
	}
	else
	{
		subList = animals.firstElementChild.firstElementChild;
		subList.innerHTML += "<li>Zorro</li>";
	}
}

addButton.addEventListener("click", addAnimal, false);
insertButton.addEventListener("click", insertAnimal, false);
replaceButton.addEventListener("click", replaceAnimal, false);
deleteButton.addEventListener("click", deleteAnimal, false);
cloneButton.addEventListener("click", cloneStructure, false);
subDomButton.addEventListener("click", addDom, false);
subHtmlButton.addEventListener("click", addHtml, false);