
// Introduce un número del 1 al 10. Genera un número aleatoriamente, si el número
// introducido en igual al generado que salga un mensaje felicitándonos y si no es así nos debe
// avisar que hemos fallado.

var jugar = document.getElementById("jugar");
var numero = document.getElementById("numero");

function juega()
{
	if (Math.floor((Math.random() * 10) + 1) == parseInt(numero.value))
	{
		alert("¡Has ganado!");
	}
	else
	{
		alert("Intentalo de nuevo...");
	}
}

jugar.addEventListener("click", juega, false);