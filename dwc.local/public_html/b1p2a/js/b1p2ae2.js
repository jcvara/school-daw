
// Crea un formulario básico con el campo dni y el botón enviar. Si el usuario deja el campo
// dni vacio saldrá un alert con un mensaje indicándonos que el campo no tiene datos al pulsar el
// botón de enviar.

var formulario = document.getElementById("formulario");
var dni = document.getElementById("dni");

function comprueba()
{
	if (dni.value.length == 0)
	{
		alert("Introduce el DNI");
		return false;
	}
}

formulario.addEventListener("submit", comprueba, false);