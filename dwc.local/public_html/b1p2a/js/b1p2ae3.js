
// Crear un caja de texto con un valor inicial de 0, y dos botones : Sumar e Inicializar. Cada
// vez que pinchemos sobre el botón Sumar se irá incrementando el valor del campo de 10 en 10,
// si pinchamos sobre el botón Inicializar el valor del campo pasará a ser 0.

var sumar = document.getElementById("sumar");
var inicializar = document.getElementById("inicializar");
var numero = document.getElementById("numero");

numero.value = 0;

function suma()
{
	numero.value = parseInt(numero.value) + 10;
}

function inicializa()
{
	numero.value = 0;
}

sumar.addEventListener("click", suma, false);
inicializar.addEventListener("click", inicializa, false);