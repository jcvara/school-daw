
// Crear una página web que nos muestre el mensaje “Bienvenido a mi página” con
// formato H1, nos muestre la plataforma del navegador que estamos utilizando, y con la
// ventana de Confirm nos pida si queremos continuar. En caso de que así sea saldrá en
// pantalla el mensaje “continuamos...”


// Creamos un fragmento de documento.
var frag = document.createDocumentFragment();

// Creamos un nuevo elemento h1 y p.
var h = document.createElement("h1");
var p = document.createElement("p");

// Creamos los nodos de texto para rellenar los elementos.
var titulo = document.createTextNode("Bienvenido a mi pagina");
var navegador = document.createTextNode("Tu navegador se identifica como: " + navigator.userAgent);

// Rellenamos los elementos.
h.appendChild(titulo);
p.appendChild(navegador);

// Insertamos los elementos en el fragmento de documento.
frag.appendChild(h);
frag.appendChild(p);

if(confirm("¿Deseas continuar?"))
{
	// Y ahora hacemos lo mismo en un solo paso, sin usar variables.
	frag.appendChild(document.createElement("p")).appendChild(document.createTextNode("Continuamos..."));
}

// Insertamos el fragmento de documento en el elemento body.
//
// De esta manera el DOM solo se tiene que actualizar una vez, haciendo el codigo
// mas eficiente.
document.body.appendChild(frag);

/*

Razones por las que no usar document.write():

- No funciona en XHTML.

- Si se ejecuta despues de finalizar la carga de la pagina, se sobreescribe el documento
  completo.

- Se ejecuta en el punto en el que se escribe, no se puede inyectar en un nodo.

- No se puede ejecutar de manera concurrente a la carga de la pagina, usando los atributos
  async o defer de la etiqueta <script>, efectivamente pausando la carga hasta que el
  codigo se haya ejecutado.

- No permite insertar el script al final de la pagina, justo antes de la etiqueta </body>,
  eliminando una buena practica para acelerar la carga de la pagina.

*/