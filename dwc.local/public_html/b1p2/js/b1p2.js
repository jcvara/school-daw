function ej01()
{

	// Introducir el precio de un articulo.
	//
	// Calcular el iva y el precio total(con el iva incluido)
	// que debe salir redondeado a dos decimales.
	//
	// Debéis utilizar una función.

	var precio;
	var iva;
	var total;

	precio = parseFloat(prompt("Introduzca el precio del articulo"));

	iva = precio * 0.21;
	total = precio + iva;

	alert(
		"Precio del articulo: " + precio.toFixed(2) + "€\n" +
		"IVA del articulo " + iva.toFixed(2) + "€\n" +
		"Precio total: " + total.toFixed(2) + "€"
	);

}

function ej02()
{

	// Introducir un mes y decir a que estación pertenece:
	//
	// Ej:
	//
	// Dime un mes: enero
	//
	// Estamos en Invierno
	//
	// (Nota: No hace falta ninguna comprobación. Los meses diciembre,enero,febrero serán
	// invierno, marzo,abril y mayo serán primavera, junio,julio y agosto serán verano y
	// septiembre,octubre y noviembre será otoño).
	//
	// Utilizar la estructura switch para resolver este ejercicio.

	var mes;

	mes = prompt("Introduce un mes:").toLowerCase();

	// Esta construccion es equivalente a decir "if (true == [Condicion en el case])"
	switch(true)
	{

		case mes == "diciembre" || mes == "enero" || mes == "febrero":

			alert("Estamos en invierno");
			break;

		case mes == "marzo" || mes == "abril" || mes == "mayo":

			alert("Estamos en primavera");
			break;

		case mes == "junio" || mes == "julio" || mes == "agosto":

			alert("Estamos en verano");
			break;

		case mes == "septiembre" || mes == "octubre" || mes == "noviembre":

			alert("Estamos en otoño");
			break;

		default:

			alert("Error: Mes introducido incorrecto");
			break;

	}

}

function ej05()
{

	// Crear una página con el botón “Fecha Actual”. Al pinchar sobre el botón debe salir la
	// fecha actual del sistema pero con el día y el mes en letra.
	//
	// Ejemplo: Lunes, 15 de Octubre de 2012

	var today = new Date();
	var weekDay = today.getDay();
	var month = today.getMonth();

	var weekDayArray = new Array("Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo");
	var monthArray = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

	alert(weekDayArray[weekDay] + ", " + today.getDate() + " de " + monthArray[month] + " de " + today.getFullYear());

}

function ej06()
{

	// Crear una página con el botón “Hora Actual”.Al pinchar sobre el botón debe salir un
	// mensaje del tipo: "Buenos días, son las 12:30 horas"
	//
	// El saludo debe variar según la hora del día. Buenos días hasta las 14:00horas, Buenas
	// tardes hasta las 20:00 y a partir de esta hora debe salir Buenas noches.

	var today = new Date();
	var hour = today.getHours();
	var minutes = today.getMinutes();
	var message = "";

	switch(true)
	{

		case hour >= 6 && hour < 14:

			message = "Buenos dias, ";
			break;

		case hour >= 14 && hour < 20:

			message = "Buenas tardes, ";

		default:

			message = "Buenas noches, ";
			break;

	}

	alert(message + "son las " + hour + ":" + minutes + " horas");

}

function ej07(color)
{

	// Crea una página con tres botones con el texto Azul,Verde y Rojo. Al pinchar sobre cada
	// botón debe cambiar el fondo de la página al color indicado. Utilizaremos el evento
	// onclick , en temas posteriores veremos otros eventos.

	document.body.style.backgroundColor = color;

}

function ej09()
{

	// Crear una página que contendrá el botón Abrir . Al pinchar sobre el botón debe aparecer
	// otra ventana que contendrá, a su vez, el botón Cerrar. Al pinchar sobre él debemos
	// volver a la página inicial.

	window.open("b1p2e9.html");

}