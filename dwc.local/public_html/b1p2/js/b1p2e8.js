
// Mostrar en pantalla un documento que nos muestre:
//
// • La URL del documento actual.
// • El pathname de la página (corresponde a la cadena que sigue al nombre del servidor).
// • El protocolo utilizado por la página web.
//
// Debéis insertar también un botón para que al pinchar sobre él se cargue la página de Google.

var frag = document.createDocumentFragment();
var boton = document.createElement("button");

boton.addEventListener("click", function(){window.location.href = 'http://www.google.es';}, false);
boton.appendChild(document.createTextNode("Google"));

frag.appendChild(document.createElement("p")).appendChild(document.createTextNode("La URL es: " + document.location));
frag.appendChild(document.createElement("p")).appendChild(document.createTextNode("El pathname es: " + document.location.pathname));
frag.appendChild(document.createElement("p")).appendChild(document.createTextNode("El protocolo es: " + document.location.protocol));
frag.appendChild(boton);

document.body.appendChild(frag);