
// Crear un programa que nos permita redimensionar la pantalla con el método resize a
// 500x500 en caso de que sea Internet Explorer, si es otro navegador que salga el
// mensaje “Operación no permitida con este navegador”.

if(navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
	var t = setTimeout("ej04()", 200);
else
	ej04();

function ej04() {

	var ancho = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	var alto = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	var nuevoAncho = 500;
	var nuevoAlto = 500;
	window.resizeBy(nuevoAncho-ancho, nuevoAlto-alto);

}