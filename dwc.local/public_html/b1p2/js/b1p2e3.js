
// Crear una página web que nos muestre el mensaje “Bienvenido a mi página” con
// formato H1, nos muestre la plataforma del navegador que estamos utilizando, y con la
// ventana de Confirm nos pida si queremos continuar. En caso de que así sea saldrá en
// pantalla el mensaje “continuamos...”

document.writeln("<h1>Bienvenido a mi pagina</h1>");
document.writeln("<p>Tu navegador se identifica como: " + navigator.userAgent + "</p>");

if(confirm("¿Deseas continuar?"))
{
	document.writeln("<p>Continuamos...</p>");
}

document.close();