
// Crea un documento que contenga 3 párrafos y estos a su vez contengan cada uno al
// menos un enlace(puedes poner en un párrafo un enlace, en otro dos y en el último tres).
//
// El documento tendrá también tres botones:
//
// Número de enlaces - Al pinchar sobre él nos mostrará el número total de enlaces que hay en
// todo el documento.
//
// Referencia enlaces - Al pinchar sobre él nos mostrará la referencia de los enlaces que hay en
// todo el documento.
//
// Referencia enlaces por párrafo - Al pinchar sobre él nos mostrará la referencia de todos los
// enlaces que hay en cada párrafo del documento.
//
// Debes utilizar funciones DOM para el acceso a los nodos. Para visualizar los datos puedes
// utilizar alert o crear un div y visualizar los datos con innerHTML.

var numberButton = document.getElementById("number");
var referenceButton = document.getElementById("reference");
var paragraphButton = document.getElementById("paragraph-reference");
var exercise = document.getElementById("exercise");
var links = document.getElementsByTagName("a");

function number()
{
	alert("Hay " + links.length + " enlaces en la pagina.");
}

function reference()
{

	var alertText = "";

	for (var i = 0; i <= links.length - 1; i++)
	{
		alertText += "Enlace " + (i+1) + ": " + links[i].href + "\n";
	};

	alert(alertText);

}

function paragraph()
{

	var alertText = "";
	var paragraphs = exercise.getElementsByTagName('p');

	for (var i = 0; i <= paragraphs.length - 1; i++)
	{

		alertText += "Parrafo " + (i+1) + "\n\n";

		for (var j = 0; j <= paragraphs[i].getElementsByTagName("a").length - 1; j++)
		{
			alertText += "Enlace " + (j+1) + ": " + paragraphs[i].getElementsByTagName("a")[j].href + "\n";
		};

		alertText += "\n";

	};

	alert(alertText);

}

numberButton.addEventListener("click", number, false);
referenceButton.addEventListener("click", reference, false);
paragraphButton.addEventListener("click", paragraph, false);