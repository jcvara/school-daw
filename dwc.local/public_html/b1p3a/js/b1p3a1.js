
// Crea un formulario básico con el campo dni y el botón enviar. Si el usuario deja el campo
// dni vacio saldrá un alert con un mensaje indicándonos que el campo no tiene datos al pulsar
// el botón de enviar. Debes utilizar funciones de DOM para acceso al campo.

var dniInput = document.getElementById("dni");
var submitButton = document.getElementById("submit");

function formCheck()
{
	if (dniInput.value === "")
	{
		alert("El campo DNI no puede estar vacio");
	}
}

submitButton.addEventListener("click", formCheck, false);