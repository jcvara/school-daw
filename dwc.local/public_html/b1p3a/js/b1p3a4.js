
// Modificar el ejercicio anterior para que contenga dos botones nuevos:
//
// Párrafos verdes- Al pinchar sobre este botón todos los párrafos del documento se pondrán en
// color verde.
//
// Desactivar color- Al pinchar sobre este botón el color de todos los párrafos volverá a ser el
// inicial.

var greenButton = document.getElementById("green");
var clearButton = document.getElementById("clear");

paragraphList = document.getElementsByTagName("p");

function green()
{

	for (var i = 0; i < paragraphList.length-1; i++)
	{
		paragraphList[i].setAttribute("class", "green");
	};

}

function clear()
{
	for (var i = 0; i < paragraphList.length-1; i++)
	{
		paragraphList[i].setAttribute("class", "");
	};
}

greenButton.addEventListener("click", green, false);
clearButton.addEventListener("click", clear, false);