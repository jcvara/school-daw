
// Crear un caja de texto con un valor inicial de 0, y dos botones : Sumar e Inicializar. Cada
// vez que pinchemos sobre el botón Sumar se irá incrementando el valor del campo de 10 en
// 10, si pinchamos sobre el botón Inicializar el valor del campo pasará a ser 0. Debes utilizar
// funciones de DOM para acceso al campo.

var numberInput = document.getElementById("number");
var addButton = document.getElementById("add");
var resetButton = document.getElementById("reset");

numberInput.value = 0;

function add()
{
	numberInput.value = parseInt(numberInput.value) + 10;
}

function reset()
{
	numberInput.value = 0;
}

addButton.addEventListener("click", add, false);
resetButton.addEventListener("click", reset, false);