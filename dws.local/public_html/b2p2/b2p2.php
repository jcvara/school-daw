<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel=”author” href=”https://plus.google.com/u/0/+JuanCarlosVaraPerez“ />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />

		<title>Bloque 2 - Parte 2</title>

	</head>

	<body>

		<div id="main-body">

			<div id="title"><h1>Bloque 2 - Parte 2</h1></div>

			<div id="body">

				<div class="exercise-container">

					<div class="exercise-title"><h2>Ejercicio 12</h2></div>

					<div class="exercise-body">

						<p><?php

							$mes = (int)date('n');
							$diaMes = (int)date('j');
							$diaSemana = (int)date('N');
							$hora = (int)date('H');
							$minuto = (int)date('i');

							$mesString = array('Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octobre', 'Novembre', 'Desembre');
							$diaString = array('Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte', 'Diumenge');
							$horaString = array('una', 'dos', 'tres', 'cuatre', 'cinc', 'sis', 'set', 'vuit', 'nou', 'deu', 'onze', 'dotze');
							$minutoString = array(
												' en punt', ' en punt', // 00-01
												'', // 02
												' y cinc', ' y cinc', ' y cinc', ' y cinc', ' y cinc', //03-07
												' y deu', ' y deu', ' y deu', ' y deu', ' y deu', // 08-12
												' y quart', ' y quart', ' y quart', ' y quart', ' y quart', // 13-17
												' y vint', ' y vint', ' y vint', ' y vint', ' y vint', //18-22
												' y vinticinc', ' y vinticinc', ' y vinticinc', ' y vinticinc', ' y vinticinc', //23-27
												' y mitja', ' y mitja', ' y mitja', ' y mitja', ' y mitja', // 28-32
												' menys vinticinc', ' menys vinticinc', ' menys vinticinc', ' menys vinticinc', ' menys vinticinc', // 33-37
												' menys vint', ' menys vint', ' menys vint', ' menys vint', ' menys vint', // 38-42
												' menys quart', ' menys quart', ' menys quart', ' menys quart', ' menys quart', // 43-47
												' menys deu', ' menys deu', ' menys deu', ' menys deu', ' menys deu', // 48-52
												' menys cinc', ' menys cinc', ' menys cinc', ' menys cinc', ' menys cinc', // 53-57
												'', '' // 58-59
												);

							if ($hora > 12)
							{
								$hora -= 12;
							}

							if ($hora === 1)
							{

								if ($minuto === 58 || $minuto === 59)
								{
									print 'Es casi la '.$horaString[$hora-1].' del '.$diaString[$diaSemana-1].', '.$diaMes.' de '.$mesString[$mes-1].'.';
								}
								else
								{
									print 'Es la '.$horaString[$hora-1].$minutoString[$minuto].' del '.$diaString[$diaSemana-1].', '.$diaMes.' de '.$mesString[$mes-1].'.';
								}

							}
							else
							{

								if ($minuto === 58 || $minuto === 59)
								{
									print 'Son casi les '.$horaString[$hora-1].' del '.$diaString[$diaSemana-1].', '.$diaMes.' de '.$mesString[$mes-1].'.';
								}
								else
								{
									print 'Son les '.$horaString[$hora-1].$minutoString[$minuto].' del '.$diaString[$diaSemana-1].', '.$diaMes.' de '.$mesString[$mes-1].'.';
								}

							}

						?></p>

					</div>

				</div><!-- #exercise-container -->

				<div class="exercise-container">

					<div class="exercise-title"><h2>Ejercicio 13</h2></div>

					<div class="exercise-body">

						<p><?php

							$alumnes['alum_1']['Llengua']=4;
							$alumnes['alum_1']['Matematiques']=5;
							$alumnes['alum_1']['Biologia']=7;
							$alumnes['alum_1']['Plastica']=4;
							$alumnes['alum_2']['Llengua']=7;
							$alumnes['alum_2']['Matematiques']=7;
							$alumnes['alum_2']['Biologia']=7;
							$alumnes['alum_2']['Plastica']=9;
							$alumnes['alum_3']['Llengua']=6;
							$alumnes['alum_3']['Matematiques']=6;
							$alumnes['alum_3']['Biologia']=5;
							$alumnes['alum_3']['Plastica']=5;

							print('La nota de alum_3 en Matematicas es: '.$alumnes['alum_3']['Matematiques']);

						?></p>

						<p><?php

							$alumnosAprobados = 3;
							$notaMedia = [];

							foreach ($alumnes as $alumnKey => $value)
							{

								$notaMedia[$alumnKey] = 0;
								$suspendido = false;

								foreach ($value as $alumnValue)
								{

									$notaMedia[$alumnKey] += $alumnValue;

									if ($alumnValue < 5 && $suspendido === false)
									{

										$alumnosAprobados -= 1;
										$suspendido = true;

									}

								}

								$notaMedia[$alumnKey] /= 4;

							}

							print('Alumnos con todo aprobado: '.$alumnosAprobados);

						?></p>

						<p><?php

							print('Nota media de alumn_1: '.$notaMedia['alum_1']);

						?></p>

						<p><?php

							print('Nota media de alumn_2: '.$notaMedia['alum_2']);

						?></p>

						<p><?php

							print('Nota media de alumn_3: '.$notaMedia['alum_3']);

						?></p>

					</div>

				</div><!-- #exercise-container -->

				<div class="exercise-container">

					<div class="exercise-title"><h2>Practica 3</h2></div>

					<div class="exercise-body">

						<?php

							$clasificacion['Alonso']['Valencia']=1;
							$clasificacion['Hamilton']['Valencia']=4;
							$clasificacion['Massa']['Valencia']=2;
							$clasificacion['Raikonen']['Valencia']=3;
							$clasificacion['Alonso']['China']=1;
							$clasificacion['Hamilton']['China']=4;
							$clasificacion['Massa']['China']=2;
							$clasificacion['Raikonen']['China']=3;
							$clasificacion['Alonso']['Brasil']=1;
							$clasificacion['Hamilton']['Brasil']=4;
							$clasificacion['Massa']['Brasil']=2;
							$clasificacion['Raikonen']['Brasil']=3;

							$puntos = [0, 10, 8, 7, 6, 4, 3, 2, 1];
							$resultado = [];

							foreach ($clasificacion as $clasKey => $value)
							{

								$resultado[$clasKey] = 0;

								foreach ($value as $clasValue)
								{

									$resultado[$clasKey] += $puntos[$clasValue];

								}

							}

							arsort($resultado);

							$numItems = count($resultado);
							$i = 0;

							foreach ($resultado as $resKey => $resValue)
							{
								print('<p>'.$resKey.' = '.$resValue.' puntos.</p>');

								if (++$i != $numItems)
								{
									print("\n\n\t\t\t\t\t\t");
								}
							}

						?>


					</div>

				</div><!-- #exercise-container -->

			</div><!-- #body -->

		</div><!-- #main-body -->

	</body>

</html>