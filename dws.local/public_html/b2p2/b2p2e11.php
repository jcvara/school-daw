<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel=”author” href=”https://plus.google.com/u/0/+JuanCarlosVaraPerez“ />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/form.css" />
		<link rel="stylesheet" type="text/css" href="../css/b2p2e11.css" />

		<title>Bloque 2.2 - Ejercicio 11</title>

	</head>

	<body>

		<div id="main-body">

			<div id="title"><h1>Bloque 2.2 - Ejercicio 11</h1></div>

			<div id="body">

				<?php

				if (isset($_POST['submit']))
				{

					$dni = $_POST['dni'];
					$letrasNif = 'TRWAGMYFPDXBNJZSQVHLCKEW';
					$nif = $dni.$letrasNif[$dni%23];

					?><p class="center"><?php print "Tu NIF es $nif"; ?>

				<?php
				}
				else
				{ ?><form action="<?php print $_SERVER[‘PHP_SELF’] ?>" method="post" accept-charset="utf-8">

					<div class="form-container">

						<div class="form-line">

							<label class="form-label" for="dni">Introduce el DNI</label>
							<input class="form-input" type="number" name="dni" id="dni" placeholder="DNI" required />

						</div>

						<div class="clear"></div>

						<div>

							<input class="submit" type="submit" name="submit" value="Enviar" />

						</div>

					</div><!-- #form-container -->

				</form>
				<?php } ?>

			</div><!-- #body -->

		</div><!-- #main-body -->

	</body>

</html>