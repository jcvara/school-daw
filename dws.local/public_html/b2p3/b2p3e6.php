<?php

	$numberString = $_POST["numberString"];
	$numberArray = explode(',', $numberString);

	function mean($numberInput)
	{
		return array_sum($numberInput) / count($numberInput);
	}

	function standard_deviation($numberInput)
	{

		if(is_array($numberInput))
		{

			foreach($numberInput as $key => $value)
			{
				$devs[$key] = pow($value - mean($numberInput), 2);
			}

			return sqrt(array_sum($devs) / (count($devs) - 1));

		}

	}

	print('Hay '.count($numberArray).' numeros. La media es '.round(mean($numberArray), 4).' y la desviacion es '.round(standard_deviation($numberArray), 4));

?>