<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez" />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/b2p3.css" />
		<link rel="stylesheet" type="text/css" href="../css/form.css" />

		<title>Bloque 3 - Practica</title>

		<script type="text/javascript" src="../js/b2p3.js" charset="utf-8" async defer></script>

	</head>

	<body>

		<div id="body-wrapper">

			<header>

				<h1 id="title">Bloque 3 - Practica</h1>

			</header>

			<nav>
				<ul>
					<li><a class="nav-link" href="b2p3.php">Ejercicios</a></li>
					<li><a class="nav-link" href="b2p3p.php">Practica</a></li>
				</ul>
			</nav>

			<div id="body">

				<div id="body-background">

					<div id="exercise-wrapper">

						<div class="exercise-container">

							<div class="exercise-title"><a name="practice"></a><h2>Practica</h2></div>

							<div class="exercise-body">

							<?php

								$files = array_diff(scandir('./../img'), array('..', '.'));

							?>

							</div>

						</div><!-- #exercise-container -->

					</div><!-- #exercise-wrapper -->

				</div><!-- #body-background -->

				<aside id="sidebar">
				</aside>

			</div><!-- #body -->

			<footer>

				<div id="credits"><p>&copy;Juan Carlos Vara Perez</p></div>

			</footer>

		</div><!-- #body-wrapper -->

	</body>

</html>