<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez" />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/b2p3.css" />
		<link rel="stylesheet" type="text/css" href="../css/form.css" />

		<title>Bloque 3 - Ejercicios</title>

		<script type="text/javascript" src="../js/b2p3.js" charset="utf-8" async defer></script>

	</head>

	<body>

		<div id="body-wrapper">

			<header>

				<h1 id="title">Bloque 3 - Ejercicios</h1>

			</header>

			<nav>
				<ul>
					<li><a class="nav-link" href="b2p3.php">Ejercicios</a></li>
					<li><a class="nav-link" href="b2p3p.php">Practica</a></li>
				</ul>
			</nav>

			<div id="body">

				<div id="body-background">

					<div id="exercise-wrapper">

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-1"></a><h2>Ejercicio 1</h2></div>

							<div class="exercise-body">

								<form id="exercise-1" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">
											<label class="form-label" for="first-side">Introduce el primer lado</label>
											<input class="form-input" type="number" name="first-side" id="first-side" placeholder="Lado 1" required />
										</div>

										<div class="form-line">
											<label class="form-label" for="second-side">Introduce el segundo lado</label>
											<input class="form-input" type="number" name="second-side" id="second-side" placeholder="Lado 2" required />
										</div>

										<div class="form-line">
											<label class="form-label" for="third-side">Introduce el tercer lado</label>
											<input class="form-input" type="number" name="third-side" id="third-side" placeholder="Lado 3" required />
										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-1" id="submit-1">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-1">
									<p id="result-1">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-2"></a><h2>Ejercicio 2</h2></div>

							<div class="exercise-body">

								<p><?php

									$numero1 = 42;
									$numero2 = 74;
									$numero3 = 0;

									function suma($numero1, $numero2, &$numero3)
									{
										$numero3 = $numero1 + $numero2;
									}

									suma($numero1, $numero2, $numero3);

									print('El resultado es: '.$numero3);

								?></p>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-3"></a><h2>Ejercicio 3</h2></div>

							<div class="exercise-body">

								<form id="exercise-3" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">
											<label class="form-label" for="number-string-3">Introduce numeros separados por comas</label>
											<input class="form-input" type="text" name="number-string-3" id="number-string-3" placeholder="Numeros" required />
										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-3" id="submit-3">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-3">
									<p id="result-3">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-4"></a><h2>Ejercicio 4</h2></div>

							<div class="exercise-body">

								<form id="exercise-4" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">
											<label class="form-label" for="factor">Introduce el numero a factorizar</label>
											<input class="form-input" type="text" name="factor" id="factor" placeholder="Numero" required />
										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-4" id="submit-4">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-4">
									<p id="result-4">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-5"></a><h2>Ejercicio 5</h2></div>

							<div class="exercise-body">

								<form id="exercise-5" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">
											<label class="form-label" for="fibonacci-elements">Introduce el numero de elementos</label>
											<input class="form-input" type="text" name="fibonacci-elements" id="fibonacci-elements" placeholder="Numero" required />
										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-5" id="submit-5">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-5">
									<p id="result-5">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-6"></a><h2>Ejercicio 6</h2></div>

							<div class="exercise-body">

								<form id="exercise-6" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">
											<label class="form-label" for="number-string-6">Introduce numeros separados por comas</label>
											<input class="form-input" type="text" name="number-string-6" id="number-string-6" placeholder="Numeros" required />
										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-6" id="submit-6">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-6">
									<p id="result-6">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

					</div><!-- #exercise-wrapper -->

				</div><!-- #body-background -->

				<aside id="sidebar">
					<ul>
						<li><a href="#exercise-1" title="Ejercicio 1">Ejercicio 1</a></li>
						<li><a href="#exercise-2" title="Ejercicio 2">Ejercicio 2</a></li>
						<li><a href="#exercise-3" title="Ejercicio 3">Ejercicio 3</a></li>
						<li><a href="#exercise-4" title="Ejercicio 4">Ejercicio 4</a></li>
						<li><a href="#exercise-5" title="Ejercicio 5">Ejercicio 5</a></li>
						<li><a href="#exercise-6" title="Ejercicio 6">Ejercicio 6</a></li>
					</ul>
				</aside>

			</div><!-- #body -->

			<footer>

				<div id="credits"><p>&copy;Juan Carlos Vara Perez</p></div>

			</footer>

		</div><!-- #body-wrapper -->

	</body>

</html>