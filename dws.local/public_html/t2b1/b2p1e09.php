<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel=”author” href=”https://plus.google.com/u/0/+JuanCarlosVaraPerez“ />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/form.css" />

		<title>Bloque 2 - Ejercicio 9</title>

	</head>

	<body>

		<div id="main-body">

			<div id="title"><h1>Bloque 2 - Ejercicio 9</h1></div>

			<div id="body">
				<?php

				if (isset($_POST['submit']))
				{

					$max = $_POST['numero'];
					$numero = 3;

					echo 'Numeros primos: 2';

					while ($numero < $max)
					{

						$cont=0;
						$raiz = floor(sqrt($numero));

						for ($i = 1; $i <= $numero; $i++)
						{

							if (($numero%$i)==0)
							{

								$cont++;
							}

							if ($i > $raiz)
							{

								break;

							}

						}

						if ($cont < 3)
						{

							echo ", ".$numero;

						}

						$numero = $numero+1;

					}

				}
				else
				{ ?>

				<form action="<?php print $_SERVER[‘PHP_SELF’] ?>" method="post" accept-charset="utf-8">

					<label for="numero">Introduce un numero</label>

					<input type="number" name="numero" id="numero" placeholder="Numero" />

					<input type="submit" name="submit" value="Enviar" />

				</form>
				<?php } ?>

			</div><!-- #body -->

		</div><!-- #main-body -->

	</body>

</html>