<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel=”author” href=”https://plus.google.com/u/0/+JuanCarlosVaraPerez“ />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/b2p1.css" />

		<title>Bloque 2 - Parte 1</title>

	</head>

	<body>

		<div id="main-body">

			<div id="title"><h1>Bloque 2 - Parte 1</h1></div>

			<div id="body">

				<div class="exercise-container">

					<div class="exercise-title"><h2>Ejercicio 1</h2></div>

					<div class="exercise-body">

						<p>Definir una variable de cada tipo: integer, double, string y boolean. Luego imprimirlas en la página, una por línea.</p>

						<p><?php
							$booleano = True;
							$entero = 76;
							$comaFlotante = 7.6;
							$cadena = 'Texto';

							echo 'Variable Boolean: '.$booleano.'<br />';
							echo 'Variable Int: '.$entero.'<br />';
							echo 'Variable Float: '.$comaFlotante.'<br />';
							echo 'Variable String: '.$cadena.'<br />';
						?></p>

					</div>

				</div>

				<div class="exercise-container">

					<div class="exercise-title"><h2>Ejercicio 2</h2></div>

					<div class="exercise-body">

						<p>Definir tres variables enteras. Luego definir un string que incorpore dichas variables y las sustituya en tiempo de ejecución. Hacedlo de las dos formas posibles, primero utilizando las comillas dobles, y luego las comillas simples, para conocer y entender las dos formas.</p>

						<p><?php
							$entero1 = 10;
							$entero2 = 20;
							$entero3 = 30;
							$cadena1 = 'El valor de $entero1 es '.$entero1.', el de $entero2 es '.$entero2.', y el de $entero3 es de '.$entero3.'.';
							$cadena2 = "El valor de ".'$entero1'." es $entero1, el de ".'$entero2'." es $entero2, y el de ".'$entero3'." es de $entero3.";

							echo $cadena1.'<br />';
							echo $cadena2.'<br />';
						?></p>

					</div>

				</div>

				<div class="exercise-container">

					<div class="exercise-title"><h2>Ejercicio 3</h2></div>

					<div class="exercise-body">

						<p>Sabiendo que la función rand() nos retorna un valor aleatorio entre un rango de dos enteros(que deberemos pasarle cuando realices la llamada a la función), debemos generar un valor aleatorio entre 1 y 3 para luego visualizar el número en letra (Ej. si se genera el 3 luego mostrar en la página el string "tres").</p>

						<p><?php

							$aleatorio = rand(1, 3);

							switch ($aleatorio)
							{
								case 1:
									echo "El numero es: Uno";
									break;

								case 2:
									echo "El numero es: Dos";
									break;

								case 3:
									echo "El numero es: Tres";
									break;

								default:
									echo 'Error inesperado';
									break;
							}
						?></p>

					</div>

				</div>

				<div class="exercise-container">

					<div class="exercise-title"><h2>Ejercicio 4</h2></div>

					<div class="exercise-body">

						<p>Sabiendo que la función rand() nos retorna un valor aleatorio entre un rango de dos enteros(que deberemos pasarle cuando realices la llamada a la función). En la variable $num se almacena un valor entero que la computadora genera en forma aleatoria entre 1 y 100. Hacer un programa que muestre por pantalla el valor generado. Mostrar además si es menor o igual a 50 o si es mayor.</p>

						<p><?php
							$num = rand(1, 100);

							echo('El numero es: '.$num.'.<br />');

							if($num <= 50)
							{
								echo('El numero es menor o igual a 50.');
							}
							else
							{
								echo('El numero es mayor que 50.');
							}
						?></p>

					</div>

				</div>

				<div class="exercise-container">

					<div class="exercise-title"><h2>Ejercicio 5</h2></div>

					<div class="exercise-body">

						<p>Mostrar la tabla de multiplicar del 2 (pares del 2 hasta el 20). Emplear el for, luego el while y por último el do/while.</p>

						<div class="ex-5">

							<p>Tabla del 2 (for)</p>

							<p><?php
								for ($i = 0; $i <= 20; $i++)
								{
									echo('2x'.$i.' = '.(2*$i).'<br />');
								}
							?></p>

						</div>

						<div class="ex-5">

							<p>Tabla del 2 (while)</p>

							<p><?php
								$i = 0;

								while ($i <= 20)
								{
									echo('2x'.$i.' = '.(2*$i).'<br />');
									$i++;
								}
							?></p>

						</div>

						<div class="ex-5">

							<p>Tabla del 2 (do-while)</p>

							<p><?php
								$i = 0;

								do
								{
									echo('2x'.$i.' = '.(2*$i).'<br />');
									$i++;
								}while($i <= 20);
							?></p>

						</div>

						<br class="clear" />

					</div>

				</div>

				<div class="exercise-container">

					<div class="exercise-title"><h2>Ejercicio 6</h2></div>

					<div class="exercise-body">

						<p>Averiguar los 15 primeros elementos de la serie de Fibonacci, donde un número es igual a la suma de los dos anteriores.</p>

						<p><?php
							$anterior1 = 1;
							$anterior2 = 1;

							echo('Serie de Fibonacci: 1, 1, ');

							for ($i = 0; $i < 14; $i++)
							{

								$fibonacci = $anterior1 + $anterior2;
								$anterior1 = $anterior2;
								$anterior2 = $fibonacci;

								echo($fibonacci);

								if ($i < 13)
								{
									echo(', ');
								}
								else
								{
									echo('.');
								}

							}
						?></p>

					</div>

				</div>

				<div class="exercise-container">

					<div class="exercise-title">

						<h2>Practica 1</h2>

					</div>

					<div class="exercise-body">

						<p>El siguiente ejercicio consiste en generar una tabla que contenga verticalmente los números del 1 al 10, que serán los divisores. Horizontalmente, tendremos 11 números a partir del número primo 23, que serán, los dividendos.</p>

						<p>Se trata de rellenar la tabla con un asterisco (*) en la intersección de los números en el caso de ser divisible, en caso contrario se rellenará con un guión (-).</p>

						<p>El color de fondo de cada casilla variará dependiendo del valor que contengan, azul para los divisibles y rojo para los no divisibles.</p>

						<p>

							<table>

								<caption>Tabla de divisibilidad</caption>

								<thead>

									<tr>
										<th>/</th>
										<th>23</th>
										<th>24</th>
										<th>25</th>
										<th>26</th>
										<th>27</th>
										<th>28</th>
										<th>29</th>
										<th>30</th>
										<th>31</th>
										<th>32</th>
										<th>33</th>
									</tr>

								</thead>

								<tbody><?php

									for ($i=1; $i <= 10; $i++)
									{
										echo '<tr><th>'.$i.'</th>';

										for ($j=23; $j <=33 ; $j++)
										{
											echo '<td'.(($j%$i == 0) ? ' class="blue">*' : ' class="red">-').'</td>';
										}

										echo '</tr>';
									}

								?></tbody>

							</table>

						</p>

					</div>

				</div>

				<!-- div.exercise-container>(div.exercise-title>h2)+div.exercise-body>p+p -->

			</div><!-- #body -->

		</div><!-- #main-body -->

	</body>

</html>