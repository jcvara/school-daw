<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel=”author” href=”https://plus.google.com/u/0/+JuanCarlosVaraPerez“ />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/form.css" />

		<title>Bloque 2 - Ejercicio 10</title>

	</head>

	<body>

		<div id="main-body">

			<div id="title"><h1>Bloque 2 - Ejercicio 10</h1></div>

			<div id="body">
				<?php

				if (isset($_POST['submit']))
				{

					$entrada = str_replace(' ', '', $_POST['palabra']);
					$palabra = strtolower($entrada);

					print $entrada.(($palabra == strrev($palabra))? '' : ' no').' es un palindromo.';

				}
				else
				{ ?>

				<form action="<?php print $_SERVER[‘PHP_SELF’] ?>" method="post" accept-charset="utf-8">

					<label for="palabra">Introduce una palabra</label>

					<input type="text" name="palabra" id="palabra" placeholder="Palabra" />

					<input type="submit" name="submit" value="Enviar" />

				</form>
				<?php } ?>

			</div><!-- #body -->

		</div><!-- #main-body -->

	</body>

</html>