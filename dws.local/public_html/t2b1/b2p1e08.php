<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel=”author” href=”https://plus.google.com/u/0/+JuanCarlosVaraPerez“ />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/form.css" />

		<title>Bloque 2 - Ejercicio 8</title>

	</head>

	<body>

		<div id="main-body">

			<div id="title"><h1>Bloque 2 - Ejercicio 8</h1></div>

			<div id="body">
				<?php

				if (isset($_POST['submit']))
				{ ?>

				<p>Bienvenido, <?php print $_POST['nombre']; ?>. Veo que<?php ($_POST['edad'] < 18) ? print ' no ' : print ' '; ?>eres mayor de edad.</p>

				<?php
				}
				else
				{ ?>

				<form action="<?php print $_SERVER[‘PHP_SELF’] ?>" method="post" accept-charset="utf-8">

					<input type="text" name="nombre" placeholder="Nombre" />
					<input type="number" min="1" max="100" step="1" name="edad" placeholder="Edad" />

					<input type="submit" name="submit" value="Enviar" />

				</form>
				<?php } ?>

			</div><!-- #body -->

		</div><!-- #main-body -->

	</body>

</html>