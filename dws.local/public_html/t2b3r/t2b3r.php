<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez" />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/form-t2b3r.css" />

		<title>Bloque  - Ejercicio </title>

		<script type="text/javascript" src="../js/t2b3r.js" charset="utf-8" async defer></script>

	</head>

	<body>

		<div id="body-wrapper">

			<header>

				<h1 id="title">Bloque  - Ejercicio </h1>

			</header>

			<nav>
				<ul>
					<li><a class="nav-link" href="t2b3r.php">Link</a></li>
					<li><a class="nav-link" href="">Link</a></li>
				</ul>
			</nav>

			<div id="body">

				<div id="body-background">

					<div id="exercise-wrapper">

						<div class="exercise-container">

							<div class="exercise-title"><h2>Ejercicio 1</h2></div>

							<div class="exercise-body">

								<form id="exercise-1" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">
											<label class="form-label" for="name">Nombre</label>
											<input class="form-input" type="text" name="name" id="name" placeholder="Nombre" required />
										</div>

										<div class="form-line">
											<label class="form-label" for="address">Direccion</label>
											<input class="form-input" type="text" name="address" id="address" placeholder="Direccion" required />
										</div>

										<div class="form-line">
											<label class="form-label" for="ingredients">Ingredientes</label>
											<input class="form-input" type="text" name="ingredients" id="ingredients" placeholder="Ingredientes" required />
										</div>

										<div class="form-line">
											<label class="form-label" for="quantity">Cantidad</label>
											<input class="form-input" type="number" name="quantity" id="quantity" placeholder="Cantidad" required />
										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-1" id="submit-1">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-1">
									<p id="result-1">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

					</div><!-- #exercise-wrapper -->

				</div><!-- #body-background -->

				<aside id="sidebar">
					<ul>
						<li><a href="#exercise-1" title="Ejercicio 1">Ejercicio 1</a></li>
						<li><a href="#exercise-2" title="Ejercicio 2">Ejercicio 2</a></li>
						<li><a href="#exercise-3" title="Ejercicio 3">Ejercicio 3</a></li>
						<li><a href="#exercise-4" title="Ejercicio 4">Ejercicio 4</a></li>
						<li><a href="#exercise-5" title="Ejercicio 5">Ejercicio 5</a></li>
					</ul>
				</aside>

			</div><!-- #body -->

			<footer>

				<div id="credits"><p>&copy;Juan Carlos Vara Perez</p></div>

			</footer>

		</div><!-- #body-wrapper -->

	</body>

</html>

