<?php

function exercise1()
{

	$fname = $_GET['fname'];
	$lname = $_GET['lname'];

	if ($fname === '' || $lname === '')
	{
		$result = 'Faltan datos...';
	}
	else
	{
		$result = 'Bienvenido, '.$fname.' '.$lname;
	}

	return $result;

}

function exercise2()
{

	$drink = $_POST['drink'];
	$quantity = $_POST['quantity'];

	$drinks = ["Coca-Cola", "Pepsi", "Fanta Naranja", "Trina Manzana"];
	$price = [1, 0.8, 0.9, 1.2];

	setlocale(LC_MONETARY, 'es_ES');

	return	'Has pedido ' . $quantity . ' unidades de ' . $drinks[$drink] .
			'. El precio total es: ' . money_format('%n', $price[$drink] * $quantity) . '€';

}

function exercise3()
{

	$quantity = $_POST['quantity'];
	$tierBreak = [10, 30]; //Se pueden añadir tantos puntos de descuento como se necesiten.
	$price = [2, 1.5, 1];  //Si se añaden mas puntos, se debe añadir el precio correspondiente.
	$tier = 0;

	foreach ($tierBreak as $key => $value)
	{

		if ($key >= 0 && $key != count($tierBreak) - 1)
		{

			if ($quantity >= 0 && $quantity <= $tierBreak[$key])
			{
				$tier = $key;
				break;
			}
			elseif ($quantity > $tierBreak[$key] && $quantity <= $tierBreak[$key + 1])
			{
				$tier = $key+1;
				break;
			}

		}
		else
		{
			$tier = count($tierBreak);
		}

	}

	setlocale(LC_MONETARY, 'es_ES');

	return	'Has pedido ' . $quantity . ' cuadernos' .
			'. El precio total es: ' . money_format('%n', $price[$tier] * $quantity) . '€';

}

function exercise4()
{

	$number_a = $_POST['numbera'];
	$number_b = $_POST['numberb'];

	return	'#1: ' . $number_a . '. #2: ' . $number_b . '. ' .
			$number_a . '+' . $number_b . '=' . ($number_a + $number_b) . '. ' .
			$number_a . '-' . $number_b . '=' . ($number_a - $number_b) . '. ' .
			$number_a . '*' . $number_b . '=' . ($number_a * $number_b) . '. ' .
			$number_a . '/' . $number_b . '=' . ($number_a / $number_b) . '. ' .
			$number_a . '%' . $number_b . '=' . ($number_a % $number_b) . '. ';

}

function exercise5()
{

	$fname = $_POST["fname"];
	$lname = $_POST["lname"];
	$address = $_POST["address"];
	$zip = $_POST["zip"];
	$city = $_POST["city"];

	return 'Hola, ' . $fname . ' ' . $lname . '. Vives en ' . $address . ', ' . $zip . ', ' . $city . '.';

}

function exercise6()
{

	$euros = $_POST['euros'];

	setlocale(LC_MONETARY, 'es_ES');

	return 'Pesetas: ' . money_format('%n', $euros * 166.386) . '.';

}

function exercise7()
{

	$euros = $_POST['euros'];
	$currency = $_POST['currency'];

	if ($currency == 'dollar')
	{

		setlocale(LC_MONETARY, 'en_US');

		return 'Dolares: ' . money_format('%n', $euros * 1.25) . '.';

	}
	else
	{

		setlocale(LC_MONETARY, 'es_ES');

		return 'Pesetas: ' . money_format('%n', $euros * 166.386) . '.';

	}

}

function exercise8()
{

	$age = $_POST['age'];
	$student = $_POST['student'];

	if($student == 'true' || $age < 12)
	{
		$price = 3.5;
	}
	else
	{
		$price = 5;
	}

	return 'Precio: ' . $price . '€';

}

function exercise9()
{

	$return =	'title=' . $_POST['title'] . "&" .
				"actors=" . $_POST['actors'] . "&" .
				"director=" . $_POST['director'] . "&" .
				"script=" . $_POST['script'] . "&" .
				"producer=" . $_POST['producer'] . "&" .
				"year=" . $_POST['year'] . "&" .
				"country=" . $_POST['country'] . "&" .
				"genre=" . $_POST['genre'] . "&" .
				"length=" . $_POST['length'] . "&" .
				"rating=" . $_POST['rating'] . "&" .
				"storyline=" . $_POST['storyline'];

	return $return;

}

if (isset($_POST['submit']))
{

	$func = 'exercise'.$_POST["exercise"];
	print($func());

}
elseif (isset($_GET['submit']))
{

	$func = 'exercise'.$_GET["exercise"];
	print($func());

}
else
{
?>

<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez" />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/form.css" />
		<link rel="stylesheet" type="text/css" href="../css/t2b4.css" />
		<link rel="stylesheet" type="text/css" href="../css/popup.css" />

		<title>Tema 2 - Bloque 4</title>

		<script src="../js/popup.js" charset="utf-8" async defer></script>
		<script src="../js/t2b4.js" charset="utf-8" async defer></script>

	</head>

	<body>

		<div id="body-wrapper">

			<header>

				<h1 id="title">Tema 2 - Bloque 4</h1>

			</header>

			<nav>
				<ul>
					<li><a class="nav-link" href=""></a></li>
				</ul>
			</nav>

			<div id="body">

				<div id="body-background">

					<div id="exercise-wrapper">

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-1"><h2>Ejercicio 1</h2></div>

							<div class="exercise-body">

								<form id="exercise-1" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">
											<label class="form-label" for="fname-1">Introduce tu nombre</label>
											<input class="form-input" type="text" name="fname-1" id="fname-1" placeholder="Nombre" />
										</div>

										<div class="form-line">
											<label class="form-label" for="lname-1">Introduce tus apellidos</label>
											<input class="form-input" type="text" name="lname-1" id="lname-1" placeholder="Apellidos" />
										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-1" id="submit-1">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-1">
									<p id="result-1">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-2"><h2>Ejercicio 2</h2></div>

							<div class="exercise-body">

								<form id="exercise-2" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">

											<label class="form-label-ex2" for="fname">Bebidas</label>

											<select name="drink" id="drink">
								                <option value="0">Coca-Cola</option>
								                <option value="1">Pepsi</option>
                								<option value="2">Fanta Naranja</option>
                								<option value="3">Trina Manzana</option>
        									</select>

        									<input type="number" id="quantity-2">

										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-2" id="submit-2">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-2">
									<p id="result-2">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-3"><h2>Ejercicio 3</h2></div>

							<div class="exercise-body">

								<form id="exercise-3" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">

											<label class="form-label-ex2" for="quantity-3">Cuadernos</label>
        									<input type="number" id="quantity-3">

										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-3" id="submit-3">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-3">
									<p id="result-3">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-4"><h2>Ejercicio 4</h2></div>

							<div class="exercise-body">

								<form id="exercise-4" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">

											<label class="form-label-ex2" for="number-4a">#1</label>
        									<input type="number" id="number-4a">

										</div>

										<div class="form-line">

											<label class="form-label-ex2" for="number-4b">#2</label>
        									<input type="number" id="number-4b">

										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-4" id="submit-4">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-4">
									<p id="result-4">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-5"><h2>Ejercicio 5</h2></div>

							<div class="exercise-body">

								<form id="exercise-5" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">

											<label class="form-label-ex9" for="fname-5">Nombre</label>
        									<input type="text" id="fname-5" placeholder="Nombre">

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="lname-5">Apellidos</label>
        									<input type="text" id="lname-5" placeholder="Apellidos">

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="address">Calle</label>
        									<input type="text" id="address" placeholder="Calle">

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="zip">Codigo Postal</label>
        									<input type="number" id="zip">

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="city">Localidad</label>
        									<input type="text" id="city" placeholder="Localidad">

										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-5" id="submit-5">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-5">
									<p id="result-5">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-6"><h2>Ejercicio 6</h2></div>

							<div class="exercise-body">

								<form id="exercise-6" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">

											<label class="form-label-ex2" for="euros-6">Euros</label>
        									<input type="number" id="euros-6">

										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-6" id="submit-6">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-6">
									<p id="result-6">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-7"><h2>Ejercicio 7</h2></div>

							<div class="exercise-body">

								<form id="exercise-7" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">

											<label class="form-label-ex2" for="euros-7">Euros</label>
        									<input type="number" id="euros-7">

										</div>

										<div class="form-line">

											<input type="radio" name="currency-7" value="pta">Pesetas<br>
											<input type="radio" name="currency-7" value="dollar">Dolares

										</div>

										<div class="clear"></div>

										<div>
											<button type="button" class="submit" name="submit-7" id="submit-7">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-7">
									<p id="result-7">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-8"><h2>Ejercicio 8</h2></div>

							<div class="exercise-body">

								<form id="exercise-8" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">

											<label class="form-label-ex2" for="age-8">Edad</label>
        									<input type="number" id="age-8">

										</div>

										<div class="form-line">

											<input type="radio" name="student-8" value="true">Estudiante<br>
											<input type="radio" name="student-8" value="false">No Estudiante

										</div>

										<div class="clear"></div>

										<div>
											<button type="reset" class="reset" id="reset-8">Borrar</button>
											<button type="button" class="submit" name="submit-8" id="submit-8">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-8">
									<p id="result-8">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-9"><h2>Ejercicio 9</h2></div>

							<div class="exercise-body">

								<form id="exercise-9" action="#" accept-charset="utf-8">

									<div class="form-container">

										<div class="form-line">

											<label class="form-label-ex9" for="movie-title">Titulo</label>
        									<input type="text" id="movie-title" placeholder="Titulo">

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="movie-actors">Actores</label>
        									<input type="text" id="movie-actors" placeholder="Actores">

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="movie-director">Director</label>
        									<input type="text" id="movie-director" placeholder="Director">

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="movie-script">Guion</label>
        									<input type="text" id="movie-script" placeholder="Guion">

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="movie-producer">Produccion</label>
        									<input type="text" id="movie-producer" placeholder="Produccion">

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="movie-year">Año</label>
        									<input type="number" id="movie-year">

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="movie-country">Nacionalidad</label>
        									<input type="text" id="movie-country" placeholder="Nacionalidad">

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="movie-genre">Genero</label>
        									<select id="movie-genre" name="movie-genre">
        										<option value="0">Comedia</option>
        										<option value="1">Drama</option>
        										<option value="2">Accion</option>
        										<option value="3">Terror</option>
        										<option value="4">Suspense</option>
        										<option value="5">Otras</option>
        									</select>

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="movie-length">Duracion</label>
        									<input type="number" id="movie-length">

										</div>

										<div class="form-line">

											<label class="form-label-ex92" for="movie-rating">Restricciones de edad</label>

										</div>

										<div class="form-line">

											<input type="radio" name="movie-rating" value="g">Todos los publicos
											<input type="radio" name="movie-rating" value="nr7">Mayores de 7 años
											<input type="radio" name="movie-rating" value="r">Mayores de 18 años

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="movie-storyline">Sinopsis</label>
        									<textarea name="movie-storyline" id="movie-storyline" cols="30" rows="10"></textarea>

										</div>

										<div class="form-line">

											<label class="form-label-ex9" for="movie-cover">Caratula</label>
        									<input type="file" id="movie-cover">

										</div>

										<div class="clear"></div>

										<div>
											<button type="reset" class="reset" id="reset-9">Borrar</button>
											<button type="button" class="submit" name="submit-9" id="submit-9">Enviar</button>
										</div>

									</div>

								</form>

								<div id="result-container-9">
									<p id="result-9">&nbsp;</p>
								</div>

							</div>

						</div><!-- #exercise-container -->

					</div><!-- #exercise-wrapper -->

				</div><!-- #body-background -->

				<aside id="sidebar">
					<ul>
						<li><a href="#exercise-1" title="Ejercicio 1">Ejercicio 1</a></li>
						<li><a href="#exercise-2" title="Ejercicio 2">Ejercicio 2</a></li>
						<li><a href="#exercise-3" title="Ejercicio 3">Ejercicio 3</a></li>
						<li><a href="#exercise-4" title="Ejercicio 4">Ejercicio 4</a></li>
						<li><a href="#exercise-5" title="Ejercicio 5">Ejercicio 5</a></li>
						<li><a href="#exercise-6" title="Ejercicio 6">Ejercicio 6</a></li>
						<li><a href="#exercise-7" title="Ejercicio 7">Ejercicio 7</a></li>
						<li><a href="#exercise-8" title="Ejercicio 8">Ejercicio 8</a></li>
						<li><a href="#exercise-9" title="Ejercicio 9">Ejercicio 9</a></li>
					</ul>
				</aside>

			</div><!-- #body -->

			<footer>

				<div id="credits"><p>&copy;Juan Carlos Vara Perez</p></div>

			</footer>

		</div><!-- #body-wrapper -->

		<div id="blanket"></div>

		<div id="popUpDiv">
			<div>
				<p>Titulo: <span id="popup-title"></span></p>
				<p>Actores: <span id="popup-actors"></span></p>
				<p>Director: <span id="popup-director"></span></p>
				<p>Guion: <span id="popup-script"></span></p>
				<p>Produccion: <span id="popup-producer"></span></p>
				<p>Año: <span id="popup-year"></span></p>
				<p>Nacionalidad: <span id="popup-country"></span></p>
				<p>Genero: <span id="popup-genre"></span></p>
				<p>Duracion: <span id="popup-length"></span></p>
				<p>Restricciones de edad: <span id="popup-rating"></span></p>
				<p>Caratula:</p>
				<p>
					<ul>
						<li>Nombre: <span id="popup-cover-name"></span></li>
						<li>Tamaño: <span id="popup-cover-size"></span></li>
						<li>Fich. Temporal: <span id="popup-cover-file"></span></li>
						<li>Tipo: <span id="popup-cover-type"></span></li>
						<li>Error: <span id="popup-cover-error"></span></li>
					</ul>
				</p>
				<p id="popup-cover"></p>
				<p>Sinopsis: <span id="popup-storyline"></span></p>
			</div>
			<button type="button" id="popup-button">Cerrar</button>
		</div>

	</body>

</html><?php }