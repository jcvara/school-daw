<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel=”author” href=”https://plus.google.com/u/0/+JuanCarlosVaraPerez“ />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/form.css" />
		<link rel="stylesheet" type="text/css" href="../css/b2p1ep.css" />

		<title>Bloque 2 - Ejercicio 11</title>

	</head>

	<body>

		<div id="main-body">

			<div id="title"><h1>Bloque 2 - Ejercicio 11</h1></div>

			<div id="body">
				<?php

				if (isset($_POST['submit']))
				{

					$capital = $_POST['capital'];
					$interes = $_POST['interes'];
					$anos = $_POST['anos'];
					$montante = $capital * pow(1 + ($interes/100), $anos);
					$meses = $anos * 12;
					$cuota = $montante / $meses;
					$interesCuota = $cuota * $interes / 100;
					$mesActual = 0;

				?><p>La cuota mensual es de <?php print round($cuota, 2); ?>€/Mes</p>

				<table id="amortizacion" summary="Tabla de amortizacion">

					<tr>

						<th>/</th>
						<th>Mes 1</th>
						<th>Mes 2</th>
						<th>Mes 3</th>
						<th>Mes 4</th>
						<th>Mes 5</th>
						<th>Mes 6</th>
						<th>Mes 7</th>
						<th>Mes 8</th>
						<th>Mes 9</th>
						<th>Mes 10</th>
						<th>Mes 11</th>
						<th>Mes 12</th>

					</tr><?php

					for ($i=0; $i < $anos; $i++)
					{

					?>

					<tr>

						<th>Año <?php print $i+1; ?></th>
						<?php

						for ($j=0; $j < 12; $j++)
						{

							$mesActual += 1;
							$c = round(($montante - ($cuota * $mesActual)) - $interesCuota, 2)

						?><td><?php print $c >= 0 ? $c : 0; ?></td>
						<?php

						}
					?>

					</tr><?php

					}?>


				</table>
				<?php

				}
				else
				{ ?>

				<form action="<?php print $_SERVER[‘PHP_SELF’] ?>" method="post" accept-charset="utf-8">

					<div class="form-container">

						<div class="form-line">

							<label class="form-label" for="capital">Introduce el capital</label>
							<input class="form-input" type="number" name="capital" id="capital" placeholder="Capital" required />

						</div>

						<div class="form-line">

							<label class="form-label" for="interes">Introduce el interes</label>
							<input class="form-input" type="number" name="interes" id="interes" placeholder="Interes" required />

						</div>

						<div  class="form-line">

							<label class="form-label" for="anos">Introduce el tiempo (en años)</label>
							<input class="form-input" type="number" name="anos" id="anos" placeholder="Años" required />

						</div>

						<div class="clear"></div>

						<div class="button">

							<input class="submit" type="submit" name="submit" value="Enviar" />

						</div>

					</div>

				</form>
				<?php } ?>

			</div><!-- #body -->

		</div><!-- #main-body -->

	</body>

</html>