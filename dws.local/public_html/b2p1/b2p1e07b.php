<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel=”author” href=”https://plus.google.com/u/0/+JuanCarlosVaraPerez“ />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/form.css" />

		<title>Bloque 2 - Ejercicio 7</title>

	</head>

	<body>

		<div id="main-body">

			<div id="title"><h1>Bloque 2 - Ejercicio 7</h1></div>

			<div id="body">

				<p>Bienvenido, <?php print $_POST['nombre']; ?>. Veo que<?php ($_POST['edad'] < 18) ? print ' no ' : print ' '; ?>eres mayor de edad.</p>

			</div><!-- #body -->

		</div><!-- #main-body -->

	</body>

</html>