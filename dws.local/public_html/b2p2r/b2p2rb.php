<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez" />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />

		<title>Bloque 2 - Parte 2 - Refuerzo</title>

	</head>

	<body>

		<div id="body-wrapper">

			<header>

				<h1 id="title">Bloque 2 - Parte 2 - Refuerzo</h1>

			</header>

			<nav>
				<ul>
					<li><a class="nav-link" href="b2p2ra.php">1 - 5</a></li>
					<li><a class="nav-link" href="b2p2rb.php">6 - 10</a></li>
					<li><a class="nav-link" href="b2p2rc.php">11 - 15</a></li>
					<li><a class="nav-link" href="b2p2rd.php">16 - 20</a></li>
				</ul>
			</nav>

			<div id="body">

				<div id="body-background">

					<div id="exercise-wrapper">

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-6"></a><h2>Ejercicio 6</h2></div>

							<div class="exercise-body">

								<?php

									$datos =	[	'Nombre' => 'Pedro Torres',
													'Direccion' => 'C\ Mayor, 47',
													'Telefono' => '988989898'
												];

									$numItems = count($datos);
									$i = 0;

									foreach ($datos as $key => $value)
									{

										print('<p>'.$key.': '.$value.'</p>');

										if (++$i != $numItems)
										{
											print("\n\n\t\t\t\t\t\t\t");
										}

									}

								?>


							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-7"></a><h2>Ejercicio 7</h2></div>

							<div class="exercise-body">

								<?php

									$ciudades = ['Madrid', 'Barcelona', 'Londres', 'New York', 'Los Angeles', 'Chicago'];

									$numItems = count($ciudades);
									$i = 0;

									foreach ($ciudades as $key => $value)
									{

										print('<p>La ciudad con el indice '.$key.' tiene el nombre de '.$value.'</p>');

										if (++$i != $numItems)
										{
											print("\n\n\t\t\t\t\t\t\t");
										}

									}

								?>


							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-8"></a><h2>Ejercicio 8</h2></div>

							<div class="exercise-body">

								<?php

									$ciudades =	[	'Uno' => 'Madrid',
													'Dos' => 'Barcelona',
													'Tres' => 'Londres',
													'Cuatro' => 'New York',
													'Cinco' => 'Los Angeles',
													'Seis' => 'Chicago'
												];

									$numItems = count($ciudades);
									$i = 0;

									foreach ($ciudades as $key => $value)
									{

										print('<p>La ciudad con el indice '.$key.' tiene el nombre de '.$value.'</p>');

										if (++$i != $numItems)
										{
											print("\n\n\t\t\t\t\t\t\t");
										}

									}

								?>


							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-9"></a><h2>Ejercicio 9</h2></div>

							<div class="exercise-body">

								<?php

									$nombres = ['Pedro', 'Ismael', 'Sonia', 'Clara', 'Susana', 'Alfonso', 'Teresa'];

									$numItems = count($nombres);
									$i = 0;

								?><p>Numero de nombres: <?php print($numItems); ?></p>

								<ul>
									<?php

									foreach ($nombres as $value)
									{

										print('<li>'.$value.'</li>');

										if (++$i != $numItems)
										{
											print("\n\t\t\t\t\t\t\t\t");
										}

									}

								?>

								</ul>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-10"></a><h2>Ejercicio 10</h2></div>

							<div class="exercise-body">

								<?php

									$lenguajes_cliente =	[	'Uno' => 'HTML',
																'Dos' => 'JavaScript',
																'Tres' => 'CSS',
																'Cuatro' => 'VRML',
																'Cinco' => 'WebGL'
															];

									$lenguajes_servidor = 	[	'One' => 'PHP',
																'Two' => 'Python',
																'Three' => 'Perl',
																'Four' => 'Ruby',
																'Five' => 'Haskell'
															];

									$lenguajes = $lenguajes_cliente + $lenguajes_servidor;

									print('<pre>');
									print_r($lenguajes);
									print("\t\t\t\t\t\t\t</pre>");

								?>


							</div>

						</div><!-- #exercise-container -->

					</div><!-- #exercise-wrapper -->

				</div>

				<aside id="sidebar">
					<ul>
						<li><a href="#exercise-6" title="Ejercicio 6">Ejercicio 6</a></li>
						<li><a href="#exercise-7" title="Ejercicio 7">Ejercicio 7</a></li>
						<li><a href="#exercise-8" title="Ejercicio 8">Ejercicio 8</a></li>
						<li><a href="#exercise-9" title="Ejercicio 9">Ejercicio 9</a></li>
						<li><a href="#exercise-10" title="Ejercicio 10">Ejercicio 10</a></li>
					</ul>
				</aside>

			</div><!-- #body -->

			<div class="clear"></div>

			<footer>

				<div id="credits"><p>&copy;Juan Carlos Vara Perez</p></div>

			</footer>

		</div><!-- #body-wrapper -->

	</body>

</html>