<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez" />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />

		<title>Bloque 2 - Parte 2 - Refuerzo</title>

	</head>

	<body>

		<div id="body-wrapper">

			<header>

				<h1 id="title">Bloque 2 - Parte 2 - Refuerzo</h1>

			</header>

			<nav>
				<ul>
					<li><a class="nav-link" href="b2p2ra.php">1 - 5</a></li>
					<li><a class="nav-link" href="b2p2rb.php">6 - 10</a></li>
					<li><a class="nav-link" href="b2p2rc.php">11 - 15</a></li>
					<li><a class="nav-link" href="b2p2rd.php">16 - 20</a></li>
				</ul>
			</nav>

			<div id="body">

				<div id="body-background">

					<div id="exercise-wrapper">

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-1"></a><h2>Ejercicio 1</h2></div>

							<div class="exercise-body">

								<?php

								$numeros = [];
								$numero = 0;
								$pares = 0;

								while ($pares < 10)
								{

									if (($numero % 2) === 0 && $numero != 0)
									{
										$numeros[$pares] = $numero;
										$pares++;
									}

									$numero++;

								}

								foreach ($numeros as $value)
								{
									print('<p>'.$value.'</p>'."\n\n\t\t\t\t\t\t");
								}

								?>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-2"></a><h2>Ejercicio 2</h2></div>

							<div class="exercise-body">

								<p><?php

									$arrayNumeros = [17, 25, 2, 84, 32, 12, 64, 42, 54, 72];
									$max = 0;

									foreach ($arrayNumeros as $value)
									{
										if ($value > $max)
										{
											$max = $value;
										}
									}

									print('El numero mas grande es: '.$max);

								?></p>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-3"></a><h2>Ejercicio 3</h2></div>

							<div class="exercise-body">

								<?php

									$v[1]=90;
									$v[30]=7;
									$v['e']=99;
									$v['hola']=43;

									$numItems = count($v);
									$i = 0;

									foreach ($v as $value)
									{
										print('<p>'.$value.'</p>');

										if (++$i != $numItems)
										{
											print("\n\n\t\t\t\t\t\t");
										}

									}

								?>


							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-4"></a><h2>Ejercicio 4</h2></div>

							<div class="exercise-body">

								<?php

									$peliculas = 	[	'Enero' => 9,
														'Febrero' => 12,
														'Marzo' => 0,
														'Abril' => 17
													];

									$numItems = count($peliculas);
									$i = 0;

									foreach ($peliculas as $key => $value)
									{
										if ($value != 0)
										{

											print('<p>En '.$key.' se han visto '.$value.' peliculas.</p>');

										}

										if (++$i != $numItems && $value != 0)
										{
											print("\n\n\t\t\t\t\t\t");
										}

									}

								?>


							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-5"></a><h2>Ejercicio 5</h2></div>

							<div class="exercise-body">

								<pre><?php

									$arrayInfo = ['Pedro', 'Ana', 34, 1];

									print_r($arrayInfo);

								?>
								</pre>

							</div>

						</div><!-- #exercise-container -->

					</div><!-- #exercise-wrapper -->

				</div><!-- #body-background -->

				<aside id="sidebar">
					<ul>
						<li><a href="#exercise-1" title="Ejercicio 1">Ejercicio 1</a></li>
						<li><a href="#exercise-2" title="Ejercicio 2">Ejercicio 2</a></li>
						<li><a href="#exercise-3" title="Ejercicio 3">Ejercicio 3</a></li>
						<li><a href="#exercise-4" title="Ejercicio 4">Ejercicio 4</a></li>
						<li><a href="#exercise-5" title="Ejercicio 5">Ejercicio 5</a></li>
					</ul>
				</aside>

			</div><!-- #body -->

			<div class="clear"></div>

			<footer>

				<div id="credits"><p>&copy;Juan Carlos Vara Perez</p></div>

			</footer>

		</div><!-- #body-wrapper -->

	</body>

</html>