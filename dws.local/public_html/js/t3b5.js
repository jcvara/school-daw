var buttons = document.getElementsByClassName("submit");
var popupButton = document.getElementById("popup-button");

var exercise = [];
var handler = [];
var id;

function ajaxIO(exercise, request, type)
{

    var xmlhttp;

    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function()
    {

        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            handler[exercise](exercise, xmlhttp.responseText);
        }

    }

    var params = type == 'GET' ? "?" + request : "";

    xmlhttp.open(type, "t3b5.php" + params, true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(request);

}

handler[0] = function(exercise, response)
{

    var oldResult = document.getElementById("result-" + exercise);

    var newResult = document.createElement("p");
    newResult.setAttribute("id", "result-" + exercise);
    newResult.appendChild(document.createTextNode(response));

    document.getElementById("result-container-" + exercise).replaceChild(newResult, oldResult);

};

handler[1] = handler[0];

handler[2] = function(exercise, response)
{

    var oldResult = document.getElementById("result-" + exercise);

    var newResult = document.createElement("p");
    newResult.setAttribute("id", "result-" + exercise);
    newResult.innerHTML = response;

    document.getElementById("result-container-" + exercise).replaceChild(newResult, oldResult);

};

exercise[1] = function()
{

    var uname = document.getElementById("uname-1");
    var pass = document.getElementById("pass-1");

    var request =   "uname=" + uname.value + "&" +
                    "pass=" + pass.value + "&" +
                    "exercise=1&submit=1";

    ajaxIO(1, request, "POST");

};

exercise[2] = function()
{

    var uname = document.getElementById("uname-2");
    var pass = document.getElementById("pass-2");
    var category = document.getElementById("category-2");

    var request =   "uname=" + uname.value + "&" +
                    "pass=" + pass.value + "&" +
                    "category=" + category.options[category.selectedIndex].value + "&" +
                    "exercise=2&submit=1";

    ajaxIO(2, request, "POST");

};

for (var i = buttons.length - 1; i >= 0; i--)
{

    id = buttons[i].id.split("-");

    if (id[0] == 'submit')
    {
        buttons[i].addEventListener("click", exercise[id[1]], false);
    }

}

popupButton.addEventListener("click", function(){popup("popUpDiv");}, false);