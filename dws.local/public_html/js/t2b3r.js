var submitButton1 = document.getElementById("submit-1");

function ajaxIO(exercise, request)
{

	var xmlhttp;

	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	}
	else
	{
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange = function()
	{

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{

			var newResult = document.createElement("p");
			newResult.setAttribute("id", "result-" + exercise);

			var oldResult = document.getElementById("result-" + exercise);

			newResult.appendChild(document.createTextNode(xmlhttp.responseText));

			document.getElementById("result-container-" + exercise).replaceChild(newResult, oldResult);

		}

	}

	xmlhttp.open("POST", "b2p3e" + exercise + ".php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(request);

}

function exercise1()
{

	var name = document.getElementById("name");
	var address = document.getElementById("address");
	var ingredients = document.getElementById("ingredients");
	var quantity = document.getElementById("quantity");

	var request =	"name=" + name.value + "&" +
					"address=" + address.value + "&" +
					"ingredients=" + ingredients.value + "&" +
					"quantity=" + quantity.value;

	ajaxIO(1, request);

}

submitButton1.addEventListener("click", exercise1, false);