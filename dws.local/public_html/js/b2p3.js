var submitButton1 = document.getElementById("submit-1");
var submitButton3 = document.getElementById("submit-3");
var submitButton4 = document.getElementById("submit-4");
var submitButton5 = document.getElementById("submit-5");
var submitButton6 = document.getElementById("submit-6");

function ajaxIO(exercise, request)
{

	var xmlhttp;

	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	}
	else
	{
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function()
	{

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{

			var newResult = document.createElement("p");
			newResult.setAttribute("id", "result-" + exercise);

			var oldResult = document.getElementById("result-" + exercise);

			newResult.appendChild(document.createTextNode(xmlhttp.responseText));

			document.getElementById("result-container-" + exercise).replaceChild(newResult, oldResult);

		}

	}

	xmlhttp.open("POST", "b2p3e" + exercise + ".php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(request);

}

function exercise1()
{

	var side1 = document.getElementById("first-side");
	var side2 = document.getElementById("second-side");
	var side3 = document.getElementById("third-side");

	var request = "side1=" + side1.value + "&" + "side2=" + side2.value + "&" + "side3=" + side3.value;

	ajaxIO(1, request);

}

function exercise3()
{

	var request = "numberString=" + document.getElementById("number-string-3").value;

	ajaxIO(3, request);

}

function exercise4()
{

	var request = "factor=" + document.getElementById("factor").value;

	ajaxIO(4, request);

}

function exercise5()
{

	var request = "elements=" + document.getElementById("fibonacci-elements").value;

	ajaxIO(5, request);

}

function exercise6()
{

	var request = "numberString=" + document.getElementById("number-string-6").value;

	ajaxIO(6, request);

}

submitButton1.addEventListener("click", exercise1, false);
submitButton3.addEventListener("click", exercise3, false);
submitButton4.addEventListener("click", exercise4, false);
submitButton5.addEventListener("click", exercise5, false);
submitButton6.addEventListener("click", exercise6, false);