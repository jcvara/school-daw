var buttons = document.getElementsByClassName("submit");
var popupButton = document.getElementById("popup-button");

var exercise = [];
var id;

function ajaxIO(exercise, request, type)
{

	var xmlhttp;

	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	}
	else
	{
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange = function()
	{

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{

			if (exercise == 9)
			{

				var response = xmlhttp.responseText;
				var elements = response.split("&");

				for (var i = elements.length - 1; i >= 0; i--)
				{
					element = elements[i].split("=");
					tag = document.getElementById("popup-" + element[0]);
					// oldResult = tag.firstElementChild;
					tag.textContent = element[1];
				};

				popup("popUpDiv");

			}
			else
			{

				var newResult = document.createElement("p");
				newResult.setAttribute("id", "result-" + exercise);

				var oldResult = document.getElementById("result-" + exercise);

				newResult.appendChild(document.createTextNode(xmlhttp.responseText));

				document.getElementById("result-container-" + exercise).replaceChild(newResult, oldResult);

			}

		}

	}

	var params = type == 'GET' ? "?" + request : "";

	xmlhttp.open(type, "t2b4.php" + params, true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(request);

}

exercise[1] = function()
{

	var fname = document.getElementById("fname-1");
	var lname = document.getElementById("lname-1");

	var request =	"fname=" + fname.value + "&" +
					"lname=" + lname.value + "&" +
					"exercise=1&submit=1";

	ajaxIO(1, request, "GET");

};

exercise[2] = function()
{

	var drink = document.getElementById("drink");
	var quantity = document.getElementById("quantity-2");

	var request =	"drink=" + drink.options[drink.selectedIndex].value + "&" +
					"quantity=" + quantity.value + "&" +
					"exercise=2&submit=1";

	ajaxIO(2, request, "POST");

};

exercise[3] = function()
{

	var quantity = document.getElementById("quantity-3");

	var request =	"quantity=" + quantity.value + "&" +
					"exercise=3&submit=1";

	ajaxIO(3, request, "POST");

};

exercise[4] = function()
{

	var number_a = document.getElementById("number-4a");
	var number_b = document.getElementById("number-4b");

	var request =	"numbera=" + number_a.value + "&" +
					"numberb=" + number_b.value + "&" +
					"exercise=4&submit=1";

	ajaxIO(4, request, "POST");

};

exercise[5] = function()
{

	var fname = document.getElementById("fname-5");
	var lname = document.getElementById("lname-5");
	var address = document.getElementById("address");
	var zip = document.getElementById("zip");
	var city = document.getElementById("city");

	var request =	"fname=" + fname.value + "&" +
					"lname=" + lname.value + "&" +
					"address=" + address.value + "&" +
					"zip=" + zip.value + "&" +
					"city=" + city.value + "&" +
					"exercise=5&submit=1";

	ajaxIO(5, request, "POST");

};

exercise[6] = function()
{

	var euros = document.getElementById("euros-6");

	var request =	"euros=" + euros.value + "&" +
					"exercise=6&submit=1";

	ajaxIO(6, request, "POST");

};

exercise[7] = function()
{

	var euros = document.getElementById("euros-7");
	var currency;

	var radios = document.getElementsByName('currency-7');

	for (var i = 0, length = radios.length; i < length; i++)
	{

		if (radios[i].checked)
		{
			currency = radios[i].value;
			break;
		}

	}

	var request =	"euros=" + euros.value + "&" +
					"currency=" + currency + "&" +
					"exercise=7&submit=1";

	ajaxIO(7, request, "POST");

};

exercise[8] = function()
{

	var age = document.getElementById("age-8");
	var student;

	var radios = document.getElementsByName('student-8');

	for (var i = 0, length = radios.length; i < length; i++)
	{

		if (radios[i].checked)
		{
			student = radios[i].value;
			break;
		}

	}

	var request =	"age=" + age.value + "&" +
					"student=" + student + "&" +
					"exercise=8&submit=1";

	ajaxIO(8, request, "POST");

};

exercise[9] = function()
{

	var title = document.getElementById("movie-title");
	var actors = document.getElementById("movie-actors");
	var director = document.getElementById("movie-director");
	var script = document.getElementById("movie-script");
	var producer = document.getElementById("movie-producer");
	var year = document.getElementById("movie-year");
	var country = document.getElementById("movie-country");
	var genre = document.getElementById("movie-genre");
	var length = document.getElementById("movie-length");
	var rating = document.getElementsByName("movie-rating");
	var storyline = document.getElementById("movie-storyline");
	var cover = document.getElementById("movie-cover");

	for (var i = 0, rlength = rating.length; i < rlength; i++)
	{

		if (rating[i].checked)
		{
			ratingValue = rating[i].value;
			break;
		}

	}

	var request = 	"title=" + title.value + "&" +
					"actors=" + actors.value + "&" +
					"director=" + director.value + "&" +
					"script=" + script.value + "&" +
					"producer=" + producer.value + "&" +
					"year=" + year.value + "&" +
					"country=" + country.value + "&" +
					"genre=" + genre.options[genre.selectedIndex].value + "&" +
					"length=" + length.value + "&" +
					"rating=" + ratingValue + "&" +
					"storyline=" + storyline.value + "&" +
					"exercise=9&submit=1";

	ajaxIO(9, request, "POST");

}

for (var i = buttons.length - 1; i >= 0; i--)
{

	id = buttons[i].id.split("-");

	if (id[0] == 'submit')
	{
		buttons[i].addEventListener("click", exercise[id[1]], false);
	}

}

popupButton.addEventListener("click", function(){popup("popUpDiv");}, false);