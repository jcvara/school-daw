<?php

	$side1 = $_POST["side1"];
	$side2 = $_POST["side2"];
	$side3 = $_POST["side3"];

	$s = (float)(($side1 + $side2 + $side3) / 2);

	$area = round(sqrt($s * ($s - $side1) * ($s - $side2) * ($s - $side3)), 4);

	is_nan($area) ? print('Las medidas introducidas no corresponden a un triangulo valido.') : print ('El area es: '.$area.' cm^2, redondeado a 4 decimales.');

?>