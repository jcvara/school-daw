<?php

	$fibonacciElements = $_POST["elements"];

	function fibonacci($number, $prev, $sum, $start)
	{

		if($number>0)
		{

			if (!$start)
			{
				print ', ';
			}

			print $prev;

			$i = $prev;
			$prev = $sum;
			$sum = $sum + $i;

			fibonacci($number-1, $prev, $sum, false);

		}

		return ;

	}

	print(fibonacci($fibonacciElements, 1, 1, true));

?>