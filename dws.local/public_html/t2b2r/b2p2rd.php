<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez" />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />

		<title>Bloque 2 - Parte 2 - Refuerzo</title>

	</head>

	<body>

		<div id="body-wrapper">

			<header>

				<h1 id="title">Bloque 2 - Parte 2 - Refuerzo</h1>

			</header>

			<nav>
				<ul>
					<li><a class="nav-link" href="b2p2ra.php">1 - 5</a></li>
					<li><a class="nav-link" href="b2p2rb.php">6 - 10</a></li>
					<li><a class="nav-link" href="b2p2rc.php">11 - 15</a></li>
					<li><a class="nav-link" href="b2p2rd.php">16 - 20</a></li>
				</ul>
			</nav>

			<div id="body">

				<div id="body-background">

					<div id="exercise-wrapper">

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-16"></a><h2>Ejercicio 16</h2></div>

							<div class="exercise-body">

								<?php

									$numeros = array(	'Uno' => 3,
														'Dos' => 2,
														'Tres' => 8,
														'Cuatro' => 123,
														'Cinco' => 5,
														'Seis' => 1
													);


								?>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-17"></a><h2>Ejercicio 17</h2></div>

							<div class="exercise-body">

								<?php

									$varios = array(	5 => 1,
														12=> 2,
														13=>56,
														x=> 42
													);

								?>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-18"></a><h2>Ejercicio 18</h2></div>

							<div class="exercise-body">

								<?php

								?>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-19"></a><h2>Ejercicio 19</h2></div>

							<div class="exercise-body">

								<?php

								?>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-20"></a><h2>Ejercicio 20</h2></div>

							<div class="exercise-body">

								<?php

								?>

							</div>

						</div><!-- #exercise-container -->

					</div><!-- #exercise-wrapper -->

				</div>

				<aside id="sidebar">
					<ul>
						<li><a href="#exercise-16" title="Ejercicio 16">Ejercicio 16</a></li>
						<li><a href="#exercise-17" title="Ejercicio 17">Ejercicio 17</a></li>
						<li><a href="#exercise-18" title="Ejercicio 18">Ejercicio 18</a></li>
						<li><a href="#exercise-19" title="Ejercicio 19">Ejercicio 19</a></li>
						<li><a href="#exercise-20" title="Ejercicio 20">Ejercicio 20</a></li>
					</ul>
				</aside>

			</div><!-- #body -->

			<div class="clear"></div>

			<footer>

				<div id="credits"><p>&copy;Juan Carlos Vara Perez</p></div>

			</footer>

		</div><!-- #body-wrapper -->

	</body>

</html>