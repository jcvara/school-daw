<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez" />

		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/table.css" />

		<title>Bloque 2 - Parte 2 - Refuerzo</title>

	</head>

	<body>

		<div id="body-wrapper">

			<header>

				<h1 id="title">Bloque 2 - Parte 2 - Refuerzo</h1>

			</header>

			<nav>
				<ul>
					<li><a class="nav-link" href="b2p2ra.php">1 - 5</a></li>
					<li><a class="nav-link" href="b2p2rb.php">6 - 10</a></li>
					<li><a class="nav-link" href="b2p2rc.php">11 - 15</a></li>
					<li><a class="nav-link" href="b2p2rd.php">16 - 20</a></li>
				</ul>
			</nav>

			<div id="body">

				<div id="body-background">

					<div id="exercise-wrapper">

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-11"></a><h2>Ejercicio 11</h2></div>

							<div class="exercise-body">

								<?php

									$naturales = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
									$impares = '';
									$pares = 0;
									$media = 0;

									$numItems = count($naturales);
									$i = 0;

									foreach ($naturales as $key => $value)
									{

										$i++;

										if ($key % 2 === 0)
										{

											$pares++;
											$media += $value;

										}
										else
										{

											$impares .= ' '.$value;

											if ($i != $numItems)
											{
												$impares .= ',';
											}

										}

									}

									print('<p>La media de los numeros en posiciones pares es: '.$media/$pares.'.</p>'."\n\n\t\t\t\t\t\t\t\t");

									print('<p>Numeros en posiciones impares:'.$impares.'.</p>'."\n");

								?>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-12"></a><h2>Ejercicio 12</h2></div>

							<div class="exercise-body">

								<?php

									$animales = ['Lagartija', 'Araña', 'Perro', 'Gato', 'Raton'];
									$numeros = [12, 34, 45, 52, 12];
									$varios = ['Sauce', 'Pino', 'Naranjo', 'Chopo', 'Perro', 34];

									$merge = array_merge($animales, $numeros, $varios);

									print('<pre>');
									print_r($merge);
									print("\t\t\t\t\t\t\t\t</pre>");

								?>


							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-13"></a><h2>Ejercicio 13</h2></div>

							<div class="exercise-body">

								<?php

									$push = [];

									foreach ($animales as $value)
									{
										array_push($push, $value);
									}

									foreach ($numeros as $value)
									{
										array_push($push, $value);
									}

									foreach ($varios as $value)
									{
										array_push($push, $value);
									}

									print('<pre>');
									print_r($push);
									print("\t\t\t\t\t\t\t\t</pre>");

								?>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-14"></a><h2>Ejercicio 14</h2></div>

							<div class="exercise-body">

								<?php

									print('<pre>');
									print_r(array_reverse($merge));
									print("\t\t\t\t\t\t\t\t</pre>");

								?>

							</div>

						</div><!-- #exercise-container -->

						<div class="exercise-container">

							<div class="exercise-title"><a name="exercise-15"></a><h2>Ejercicio 15</h2></div>

							<div class="exercise-body">

								<?php

									$estadios_futbol = array(	'Barcelona' => 'Camp Nou',
																'Real Madrid' => 'Santiago Bernabeu',
																'Valencia' => 'Mestalla',
																'Real Sociedad' => 'Anoeta'
															);

								?><table>

									<thead>
										<tr>
											<th>Indice</th>
											<th>Valor</th>
										</tr>
									</thead>

									<tbody>
										<?php

											$numItems = count($estadios_futbol);
											$i = 0;

											foreach ($estadios_futbol as $key => $value)
											{
												print('<tr>'."\n\t\t\t\t\t\t\t\t\t\t\t");
												print('<td>'.$key.'</td>'."\n\t\t\t\t\t\t\t\t\t\t\t");
												print('<td>'.$value.'</td>'."\n\t\t\t\t\t\t\t\t\t\t");
												print('</tr>');

												if (++$i != $numItems)
												{
													print("\n\t\t\t\t\t\t\t\t\t\t");
												}
												else
												{
													print("\n");
												}

											}

										?>
									</tbody>

								</table>

								<?php

									unset($estadios_futbol['Real Madrid']);

								?><table>

									<thead>
										<tr>
											<th>Indice</th>
											<th>Valor</th>
										</tr>
									</thead>

									<tbody>
										<?php

											$numItems = count($estadios_futbol);
											$i = 0;

											foreach ($estadios_futbol as $key => $value)
											{
												print('<tr>'."\n\t\t\t\t\t\t\t\t\t\t\t");
												print('<td>'.$key.'</td>'."\n\t\t\t\t\t\t\t\t\t\t\t");
												print('<td>'.$value.'</td>'."\n\t\t\t\t\t\t\t\t\t\t");
												print('</tr>');

												if (++$i != $numItems)
												{
													print("\n\t\t\t\t\t\t\t\t\t\t");
												}
												else
												{
													print("\n");
												}

											}

										?>
									</tbody>

								</table>

							</div>

						</div><!-- #exercise-container -->

					</div><!-- #exercise-wrapper -->

				</div>

				<aside id="sidebar">
					<ul>
						<li><a href="#exercise-11" title="Ejercicio 11">Ejercicio 11</a></li>
						<li><a href="#exercise-12" title="Ejercicio 12">Ejercicio 12</a></li>
						<li><a href="#exercise-13" title="Ejercicio 13">Ejercicio 13</a></li>
						<li><a href="#exercise-14" title="Ejercicio 14">Ejercicio 14</a></li>
						<li><a href="#exercise-15" title="Ejercicio 15">Ejercicio 15</a></li>
					</ul>
				</aside>

			</div><!-- #body -->

			<div class="clear"></div>

			<footer>

				<div id="credits"><p>&copy;Juan Carlos Vara Perez</p></div>

			</footer>

		</div><!-- #body-wrapper -->

	</body>

</html>