<?php

require_once('mysql.php');

function exercise1()
{

  $uname = $_POST['uname'];
  $pass = $_POST['pass'];

  if ($uname === '' || $pass === '')
  {
    $result = 'Faltan datos...';
  }
  else
  {

    $db = new Mysql;
    $pdo = $db->getPDO('localhost', 'root', 'Mankeulv#9288', false);

    $sql = 'CREATE USER :user@localhost IDENTIFIED BY :pass';

    $statement = $pdo->prepare($sql);
    $statement->bindValue(':user', $uname);
    $statement->bindValue(':pass', $pass);

    if(!$statement->execute())
    {
      return 'Error al crear el usuario en la base de datos';
    }

    $sql = 'GRANT SELECT ON gestion . * TO :user@localhost';

    $statement = $pdo->prepare($sql);
    $statement->bindValue(':user', $uname);

    if(!$statement->execute())
    {
      return 'Error al asignar permisos al usuario en la base de datos';
    }

    $result = 'Nuevo usuario creado correctamente.';

  }

  return $result;

}

function exercise2()
{

  $uname = $_POST['uname'];
  $pass = $_POST['pass'];
  $category = $_POST['category'];
  $categories = ['herramientas', 'tornilleria'];

  if ($uname === '' || $pass === '')
  {
    $result = 'Usuario o contraseña vacios...';
  }
  else
  {



    $db = new Mysql;
    $pdo = $db->getPDO('localhost', $uname, $pass, 'gestion');

    $sql = 'SELECT * FROM productos WHERE categoria = :category';

    $statement = $pdo->prepare($sql);
    $statement->bindValue(':category', $categories[$category]);

    if(!$statement->execute())
    {
      return 'Error al ejecutar la consulta';
    }

    $result =   '<table>' .
                  '<thead>' .
                    '<tr>'.
                      '<th>Cod. Producto</th>' .
                      '<th>Categoria</th>' .
                      '<th>Descripcion</th>' .
                      '<th>Cod. Proveedor</th>' .
                      '<th>Precio Compra</th>' .
                      '<th>Precio Venta</th>' .
                      '<th>Stock</th>' .
                    '</tr>' .
                  '</thead>' .
                  '<tbody>';

    while($row = $statement->fetch(PDO::FETCH_ASSOC))
    {

      $result .= '<tr>';

      foreach ($row as $value)
      {
        $result .= '<td>' . $value . '</td>';
      }

      $result .= '</tr>';

    }

    $result .=  '</tbody>' .
              '</table>';
;

  }

  return $result;

}

if (isset($_POST['submit']))
{

  $func = 'exercise'.$_POST["exercise"];
  print($func());

}
else
{
?>

<!DOCTYPE html>

<html>

  <head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="description" content="Ejercicios Bloque 1" />

    <meta name="author" content="Juan Carlos Vara Perez" />
    <link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez" />

    <link rel="stylesheet" type="text/css" href="../css/common.css" />
    <link rel="stylesheet" type="text/css" href="../css/form.css" />
    <link rel="stylesheet" type="text/css" href="../css/popup.css" />
    <link rel="stylesheet" type="text/css" href="../css/table.css" />

    <title>Tema 2 - Bloque 4</title>

    <script src="../js/popup.js" charset="utf-8" async defer></script>
    <script src="../js/t3b5.js" charset="utf-8" async defer></script>

  </head>

  <body>

    <div id="body-wrapper">

      <header>

        <h1 id="title">Tema 3 - Bloque 5</h1>

      </header>

      <nav>
        <ul>
          <li><a class="nav-link" href=""></a></li>
        </ul>
      </nav>

      <div id="body">

        <div id="body-background">

          <div id="exercise-wrapper">

            <div class="exercise-container">

              <div class="exercise-title"><a name="exercise-1"></a><h2>Registro</h2></div>

              <div class="exercise-body">

                <form id="exercise-1" action="#" accept-charset="utf-8">

                  <div class="form-container">

                    <div class="form-line">
                      <label class="form-label" for="uname-1">Nombre de Usuario</label>
                      <input class="form-input" type="text" name="uname-1" id="uname-1" placeholder="Usuario" />
                    </div>

                    <div class="form-line">
                      <label class="form-label" for="pass-1">Contraseña</label>
                      <input class="form-input" type="password" name="pass-1" id="pass-1" placeholder="Contraseña" />
                    </div>

                    <div class="clear"></div>

                    <div>
                      <button type="reset" class="reset" id="reset-1">Borrar</button>
                      <button type="button" class="submit" name="submit-1" id="submit-1">Enviar</button>
                    </div>

                  </div>

                </form>

                <div id="result-container-1">
                  <p id="result-1">&nbsp;</p>
                </div>

              </div>

            </div><!-- #exercise-container -->

            <div class="exercise-container">

              <div class="exercise-title"><a name="exercise-2"></a><h2>Login</h2></div>

              <div class="exercise-body">

                <form id="exercise-2" action="#" accept-charset="utf-8">

                  <div class="form-container">

                    <div class="form-line">
                      <label class="form-label" for="uname-2">Nombre de Usuario</label>
                      <input class="form-input" type="text" name="uname-1" id="uname-2" placeholder="Usuario" />
                    </div>

                    <div class="form-line">
                      <label class="form-label" for="pass-2">Contraseña</label>
                      <input class="form-input" type="password" name="pass-2" id="pass-2" placeholder="Contraseña" />
                    </div>

                    <div class="form-line">
                      <label class="form-label" for="category-2">Categoria</label>
                      <select class="form-input" name="category-2" id="category-2">
                        <option value="0">Herramientas</option>
                        <option value="1">Tornilleria</option>
                      </select>
                    </div>

                    <div class="clear"></div>

                    <div>
                      <button type="reset" class="reset" id="reset-2">Borrar</button>
                      <button type="button" class="submit" name="submit-2" id="submit-2">Enviar</button>
                    </div>

                  </div>

                </form>

                <div id="result-container-2">
                  <p id="result-2">&nbsp;</p>
                </div>

              </div>

            </div><!-- #exercise-container -->

          </div><!-- #exercise-wrapper -->

        </div><!-- #body-background -->

        <aside id="sidebar">
          <!-- <ul>
            <li><a href="#exercise-1" title="Ejercicio 1">Ejercicio 1</a></li>
          </ul> -->
        </aside>

      </div><!-- #body -->

      <footer>

        <div id="credits"><p>&copy;Juan Carlos Vara Perez</p></div>

      </footer>

    </div><!-- #body-wrapper -->

    <div id="blanket"></div>

    <div id="popUpDiv">
      <div>
        <p>Titulo: <span id="popup-title"></span></p>
        <p>Actores: <span id="popup-actors"></span></p>
        <p>Director: <span id="popup-director"></span></p>
        <p>Guion: <span id="popup-script"></span></p>
        <p>Produccion: <span id="popup-producer"></span></p>
        <p>Año: <span id="popup-year"></span></p>
        <p>Nacionalidad: <span id="popup-country"></span></p>
        <p>Genero: <span id="popup-genre"></span></p>
        <p>Duracion: <span id="popup-length"></span></p>
        <p>Restricciones de edad: <span id="popup-rating"></span></p>
        <p>Caratula:</p>
        <p>
          <ul>
            <li>Nombre: <span id="popup-cover-name"></span></li>
            <li>Tamaño: <span id="popup-cover-size"></span></li>
            <li>Fich. Temporal: <span id="popup-cover-file"></span></li>
            <li>Tipo: <span id="popup-cover-type"></span></li>
            <li>Error: <span id="popup-cover-error"></span></li>
          </ul>
        </p>
        <p id="popup-cover"></p>
        <p>Sinopsis: <span id="popup-storyline"></span></p>
      </div>
      <button type="button" id="popup-button">Cerrar</button>
    </div>

  </body>

</html><?php }