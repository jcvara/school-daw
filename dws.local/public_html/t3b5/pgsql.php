<?php

class Pgsql
{

    private $pdo=null;

    public function getPDO($host, $username, $password, $db)
    {

        if(!$this->pdo)
        {

            try
            {
                $this->pdo = new PDO('pgsql:host=' . $host . ';user=' . $username . ';password=' . $password . ';dbname=' . $db);
            }
            catch (PDOException $e)
            {
                die($e->getMessage());
            }

        }

        return $this->pdo;

    }

}
